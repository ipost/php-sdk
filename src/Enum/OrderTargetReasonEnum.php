<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self REASON_NONE()                         Нет причины/Не задано
 * @method static self REASON_CUSTOM()                       Другая причина (в reason_custom)
 * @method static self REASON_REFUSED()                      По назначению получен отказ (отправитель/получатель отказался отправить/получить посылку)
 * @method static self REASON_CLIENT_CANCELED()              Назначение отменено клиентом
 * @method static self REASON_CALL_NOT_ANSWERED()            Курьер не дозвонился отправителю/получателю. / Отправитель/получатель не вышел на связь
 * @method static self REASON_AGE_CHECK_NOT_PASSED()         Получатель не прошел проверку возраста (совершеннолетие) // применяется только к причине в пункте ДОСТАВКИ
 * @method static self REASON_PERSONAL_ID_CHECK_NOT_PASSED() Получатель не прошел сверку документов (ИНН) // применяется только к причине в пункте ДОСТАВКИ
 */
class OrderTargetReasonEnum extends Enum
{
    private const REASON_NONE = 0;
    private const REASON_CUSTOM = 1;
    private const REASON_REFUSED = 2;
    private const REASON_CLIENT_CANCELED = 3;
    private const REASON_CALL_NOT_ANSWERED = 4;
    private const REASON_AGE_CHECK_NOT_PASSED = 5;
    private const REASON_PERSONAL_ID_CHECK_NOT_PASSED = 6;

    public function label(): string
    {
        switch ($this) {
            case self::REASON_NONE():
                return 'Нет причины';
            case self::REASON_CUSTOM():
                return 'Другая причина';
            case self::REASON_REFUSED():
                return 'По назначению получен отказ';
            case self::REASON_CLIENT_CANCELED():
                return 'Назначение отменено клиентом';
            case self::REASON_CALL_NOT_ANSWERED():
                return 'Курьер не дозвонился отправителю / получателю';
            case self::REASON_AGE_CHECK_NOT_PASSED():
                return 'Получатель не прошел проверку возраста';
            case self::REASON_PERSONAL_ID_CHECK_NOT_PASSED():
                return 'Получатель не прошел сверку документов (ИНН)';
            default:
                throw new \UnexpectedValueException();
        }
    }
}