<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self NONE()     Задача не начата
 * @method static self ONWAY()    Курьер в пути к точке назначения
 * @method static self WAIT()     Курьер ожидает отправителя / получателя
 * @method static self DONE()     Назначение завершено
 * @method static self CANCELED() Назначение отменено (отмена клиентом, отказ, отправитель/получатель не вышел на связь)
 */
class NotifyOrderTargetStatusEnum extends Enum
{
    private const NONE = 'NONE';
    private const ONWAY = 'ONWAY';
    private const WAIT = 'WAIT';
    private const DONE = 'DONE';
    private const CANCELED = 'CANCELED';

    public function label(): string
    {
        switch ($this) {
            case self::NONE():
                return 'Задача не начата';
            case self::ONWAY():
                return 'Курьер в пути к точке назначения';
            case self::WAIT():
                return 'Курьер ожидает отправителя / получателя';
            case self::DONE():
                return 'Назначение завершено';
            case self::CANCELED():
                return 'Назначение отменено';
            default:
                throw new \UnexpectedValueException();
        }
    }

    public function origin(): OrderTargetStatusEnum
    {
        return OrderTargetStatusEnum::{'STATUS_' . $this->getValue()}();
    }
}