<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self TYPE_CLIENT()    Клиент
 * @method static self TYPE_RECIPIENT() Получатель
 */
class CourierReviewAuthorTypeEnum extends Enum
{
    private const TYPE_CLIENT = 1;
    private const TYPE_RECIPIENT = 2;

    public function label(): string
    {
        switch ($this) {
            case self::TYPE_CLIENT():
                return 'Клиент';
            case self::TYPE_RECIPIENT():
                return 'Получатель';
            default:
                throw new \UnexpectedValueException();
        }
    }
}