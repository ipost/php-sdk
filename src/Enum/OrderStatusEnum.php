<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self STATUS_NEW()       Поиск курьера
 * @method static self STATUS_INWORK()    Заказ находится в работе
 * @method static self STATUS_MODERATE()  Заказ находится на модерации
 * @method static self STATUS_UNAPPLIED() Курьер не назначен
 * @method static self STATUS_DONE()      Заказ завершен
 * @method static self STATUS_CANCELED()  Заказ отменен
 */
class OrderStatusEnum extends Enum
{
    private const STATUS_NEW = 0;
    private const STATUS_INWORK = 1;
    private const STATUS_MODERATE = 3;
    private const STATUS_UNAPPLIED = -1;
    private const STATUS_DONE = -2;
    private const STATUS_CANCELED = -4;

    public function label(): string
    {
        switch ($this) {
            case self::STATUS_NEW():
                return 'Поиск курьера';
            case self::STATUS_INWORK():
                return 'Заказ находится в работе';
            case self::STATUS_MODERATE():
                return 'Заказ находится на модерации';
            case self::STATUS_UNAPPLIED():
                return 'Курьер не назначен';
            case self::STATUS_DONE():
                return 'Заказ завершен';
            case self::STATUS_CANCELED():
                return 'Заказ отменен';
            default:
                throw new \UnexpectedValueException();
        }
    }
}