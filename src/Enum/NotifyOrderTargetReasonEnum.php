<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self CUSTOM()                       Другая причина. Указана в reason_custom.
 * @method static self REFUSED()                      По назначению получен отказ (отправитель/получатель отказался отправить/получить посылку)
 * @method static self CLIENT_CANCELED()              Назначение отменено клиентом
 * @method static self CALL_NOT_ANSWERED()            Курьер не дозвонился отправителю/получателю. / Отправитель/получатель не вышел на связь
 * @method static self AGE_CHECK_NOT_PASSED()         Получатель не прошел проверку возраста (совершеннолетие) // применяется только к причине в пункте ДОСТАВКИ
 * @method static self PERSONAL_ID_CHECK_NOT_PASSED() Получатель не прошел сверку документов (ИНН) // применяется только к причине в пункте ДОСТАВКИ
 */
class NotifyOrderTargetReasonEnum extends Enum
{
    private const CUSTOM = 'CUSTOM';
    private const REFUSED = 'REFUSED';
    private const CLIENT_CANCELED = 'CLIENT_CANCELED';
    private const CALL_NOT_ANSWERED = 'CALL_NOT_ANSWERED';
    private const AGE_CHECK_NOT_PASSED = 'AGE_CHECK_NOT_PASSED';
    private const PERSONAL_ID_CHECK_NOT_PASSED = 'PERSONAL_ID_CHECK_NOT_PASSED';

    public function label(): string
    {
        switch ($this) {
            case self::CUSTOM():
                return 'Другая причина';
            case self::REFUSED():
                return 'По назначению получен отказ';
            case self::CLIENT_CANCELED():
                return 'Назначение отменено клиентом';
            case self::CALL_NOT_ANSWERED():
                return 'Курьер не дозвонился отправителю / получателю';
            case self::AGE_CHECK_NOT_PASSED():
                return 'Получатель не прошел проверку возраста';
            case self::PERSONAL_ID_CHECK_NOT_PASSED():
                return 'Получатель не прошел сверку документов (ИНН)';
            default:
                throw new \UnexpectedValueException();
        }
    }

    public function origin(): OrderTargetReasonEnum
    {
        return OrderTargetReasonEnum::{'REASON_' . $this->getValue()}();
    }
}