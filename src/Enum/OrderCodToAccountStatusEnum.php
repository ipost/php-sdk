<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self STATUS_NONE()     Задача не начата / Курьер еще не завершил доставку посылки
 * @method static self STATUS_AWAITING() Ожидание зачисления наложенного платежа
 * @method static self STATUS_DONE()     Наложенный платеж успешно зачислен
 * @method static self STATUS_CANCELED() Наложенный платеж отменен (например, в случае, если доставка была отменена / получатель отказался принять посылку и т.п.)
 */
class OrderCodToAccountStatusEnum extends Enum
{
    private const STATUS_NONE = 0;
    private const STATUS_AWAITING = 1;
    private const STATUS_DONE = 2;
    private const STATUS_CANCELED = 3;

    public function label(): string
    {
        switch ($this) {
            case self::STATUS_NONE():
                return 'Курьер еще не завершил доставку посылки';
            case self::STATUS_AWAITING():
                return 'Ожидание зачисления наложенного платежа';
            case self::STATUS_DONE():
                return 'Наложенный платеж успешно зачислен';
            case self::STATUS_CANCELED():
                return 'Наложенный платеж отменен';
            default:
                throw new \UnexpectedValueException();
        }
    }
}