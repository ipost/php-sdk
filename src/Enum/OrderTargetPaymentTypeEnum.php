<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self PAYMENT_TYPE_BALANCE()   Оплата с баланса отправителем
 * @method static self PAYMENT_TYPE_SENDER()    Оплата наличными отправителем
 * @method static self PAYMENT_TYPE_RECIPIENT() Оплата наличными получателем
 * @method static self PAYMENT_TYPE_CARD()      Оплата с карты
 */
class OrderTargetPaymentTypeEnum extends Enum
{
    private const PAYMENT_TYPE_BALANCE = 1;
    private const PAYMENT_TYPE_SENDER = 2;
    private const PAYMENT_TYPE_RECIPIENT = 3;
    private const PAYMENT_TYPE_CARD = 7;

    public function label(): string
    {
        switch ($this) {
            case self::PAYMENT_TYPE_BALANCE():
                return 'Оплата с баланса отправителем';
            case self::PAYMENT_TYPE_SENDER():
                return 'Оплата наличными отправителем';
            case self::PAYMENT_TYPE_RECIPIENT():
                return 'Оплата наличными получателем';
            case self::PAYMENT_TYPE_CARD():
                return 'Оплата с карты';
            default:
                throw new \UnexpectedValueException();
        }
    }
}