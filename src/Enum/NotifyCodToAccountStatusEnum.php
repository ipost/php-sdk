<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self NONE()     Задача не начата / Курьер еще не завершил доставку посылки
 * @method static self AWAITING() Ожидание зачисления наложенного платежа
 * @method static self DONE()     Наложенный платеж успешно зачислен
 * @method static self CANCELED() Наложенный платеж отменен. Например, в случае, если доставка была отменена / получатель отказался принять посылку и т.п.
 */
class NotifyCodToAccountStatusEnum extends Enum
{
    private const NONE = 'NONE';
    private const AWAITING = 'AWAITING';
    private const DONE = 'DONE';
    private const CANCELED = 'CANCELED';

    public function label(): string
    {
        switch ($this) {
            case self::NONE():
                return 'Курьер еще не завершил доставку посылки';
            case self::AWAITING():
                return 'Ожидание зачисления наложенного платежа';
            case self::DONE():
                return 'Наложенный платеж успешно зачислен';
            case self::CANCELED():
                return 'Наложенный платеж отменен.';
            default:
                throw new \UnexpectedValueException();
        }
    }

    public function origin(): OrderCodToAccountStatusEnum
    {
        return OrderCodToAccountStatusEnum::{'STATUS_' . $this->getValue()}();
    }
}