<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self REASON_NONE()                 Нет причины / Все хорошо
 * @method static self REASON_CANCELED_BY_CLIENT()   Заказ отменен клиентом
 * @method static self REASON_CANCELED_BY_COURIER()  Заказ отменен курьером
 * @method static self REASON_TARGET_CANCELED()      Одна из точек (pickup, deliveries[], redelivery) была отменена, из-за чего невозможно продолжить исполнение заказа
 * @method static self REASON_MANUAL_REVIEW()        Заказ на ручной проверке
 * @method static self REASON_FRAUD()                Подозрение на мошенничество
 * @method static self REASON_COURIER_NOT_ASSIGNED() Курьер не назначен
 * @method static self REASON_PAYMENT_FAILED()       Неудачная оплата с карты
 */
class OrderReasonEnum extends Enum
{
    private const REASON_NONE = 0;
    private const REASON_CANCELED_BY_CLIENT = 1;
    private const REASON_CANCELED_BY_COURIER = 2;
    private const REASON_TARGET_CANCELED = 3;
    private const REASON_MANUAL_REVIEW = 4;
    private const REASON_FRAUD = 5;
    private const REASON_COURIER_NOT_ASSIGNED = 6;
    private const REASON_PAYMENT_FAILED = 7;

    public function label(): string
    {
        switch ($this) {
            case self::REASON_NONE():
                return 'Нет причины';
            case self::REASON_CANCELED_BY_CLIENT():
                return 'Заказ отменен клиентом';
            case self::REASON_CANCELED_BY_COURIER():
                return 'Заказ отменен курьером';
            case self::REASON_TARGET_CANCELED():
                return 'Одна из точек была отменена';
            case self::REASON_MANUAL_REVIEW():
                return 'Заказ на ручной проверке';
            case self::REASON_FRAUD():
                return 'Подозрение на мошенничество';
            case self::REASON_COURIER_NOT_ASSIGNED():
                return 'Курьер не назначен';
            case self::REASON_PAYMENT_FAILED():
                return 'Неудачная оплата с карты';
            default:
                throw new \UnexpectedValueException();
        }
    }
}