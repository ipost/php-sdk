<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self REDELIVERY_TYPE_NONE()    Без обратной доставки
 * @method static self REDELIVERY_TYPE_REGULAR() Обычная обратная доставка по тарифу
 * @method static self REDELIVERY_TYPE_COD()     Обратная доставка наложенного платежа (совместно с OrderDeliveryFinanceTypeEnum(FINANCE_TYPE_COD_REDELIVERY))
 */
class OrderDeliveryRedeliveryTypeEnum extends Enum
{
    private const REDELIVERY_TYPE_NONE = 0;
    private const REDELIVERY_TYPE_REGULAR = 1;
    private const REDELIVERY_TYPE_COD = 2;

    public function label(): string
    {
        switch ($this) {
            case self::REDELIVERY_TYPE_NONE():
                return 'Без обратной доставки';
            case self::REDELIVERY_TYPE_REGULAR():
                return 'Обычная обратная доставка по тарифу';
            case self::REDELIVERY_TYPE_COD():
                return 'Обратная доставка наложенного платежа';
            default:
                throw new \UnexpectedValueException();
        }
    }
}