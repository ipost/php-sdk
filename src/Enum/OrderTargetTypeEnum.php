<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self TARGET_NONE()       Нет назначения
 * @method static self TARGET_PICKUP()     Забор посылки
 * @method static self TARGET_DELIVERY()   Доставка
 * @method static self TARGET_REDELIVERY() Обратная доставка
 * @method static self TARGET_COD()        Наложенный платеж
 */
class OrderTargetTypeEnum extends Enum
{
    private const TARGET_NONE = 0;
    private const TARGET_PICKUP = 1;
    private const TARGET_DELIVERY = 2;
    private const TARGET_REDELIVERY = 3;
    private const TARGET_COD = 4;

    public function label(): string
    {
        switch ($this) {
            case self::TARGET_NONE():
                return 'Нет назначения';
            case self::TARGET_PICKUP():
                return 'Забор посылки';
            case self::TARGET_DELIVERY():
                return 'Доставка посылки';
            case self::TARGET_REDELIVERY():
                return 'Обратная доставка';
            case self::TARGET_COD():
                return 'Наложенный платеж';
            default:
                throw new \UnexpectedValueException();
        }
    }
}