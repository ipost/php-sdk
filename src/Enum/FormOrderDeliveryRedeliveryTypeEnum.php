<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self REDELIVERY_TYPE_NONE()    Без обратной доставки
 * @method static self REDELIVERY_TYPE_REGULAR() Обычная обратная доставка по тарифу
 */
class FormOrderDeliveryRedeliveryTypeEnum extends Enum
{
    private const REDELIVERY_TYPE_NONE = 0;
    private const REDELIVERY_TYPE_REGULAR = 1;

    public function label(): string
    {
        switch ($this) {
            case self::REDELIVERY_TYPE_NONE():
                return 'Без обратной доставки';
            case self::REDELIVERY_TYPE_REGULAR():
                return 'Обычная обратная доставка по тарифу';
            default:
                throw new \UnexpectedValueException();
        }
    }
}