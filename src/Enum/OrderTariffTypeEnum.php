<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self INCITY()    Доставка по городу
 * @method static self INTERCITY() Междугородняя доставка
 * @method static self MULTI()     Мульти-доставка (несколько получателей) по городу
 */
class OrderTariffTypeEnum extends Enum
{
    private const INCITY = 1;
    private const INTERCITY	= 2;
    private const MULTI = 3;

    public function label(): string
    {
        switch ($this) {
            case self::INCITY():
                return 'Доставка по городу';
            case self::INTERCITY():
                return 'Междугородняя доставка';
            case self::MULTI():
                return 'Мульти-доставка по городу';
            default:
                throw new \UnexpectedValueException();
        }
    }
}