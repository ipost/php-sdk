<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self FINANCE_TYPE_NONE()             Нет доп. фин. операций
 * @method static self FINANCE_TYPE_REDEEM_BALANCE()   Выкупить посылку у отправителя, стоимость выкупа будет компенсирована с баланса клиента
 * @method static self FINANCE_TYPE_REDEEM_RECIPIENT() Выкупить посылку у отправителя, стоимость выкупа компенсирует получатель
 * @method static self FINANCE_TYPE_COD_BALANCE()      Наложенный платеж: перевод суммы на счет клиента
 * @method static self FINANCE_TYPE_COD_CARD()         Наложенный платеж: перевод суммы на привязанную карту клиента
 * @method static self FINANCE_TYPE_COD_TOACCOUNT()    Наложенный платеж: на расчетный счет
 */
class FormOrderDeliveryFinanceTypeEnum extends Enum
{
    private const FINANCE_TYPE_NONE = 0;
    private const FINANCE_TYPE_REDEEM_BALANCE = 1;
    private const FINANCE_TYPE_REDEEM_RECIPIENT = 2;
    private const FINANCE_TYPE_COD_BALANCE = 3;
    private const FINANCE_TYPE_COD_CARD = 6;
    private const FINANCE_TYPE_COD_TOACCOUNT = 7;

    public function label(): string
    {
        switch ($this) {
            case self::FINANCE_TYPE_NONE():
                return 'Нет доп. фин. операций';
            case self::FINANCE_TYPE_REDEEM_BALANCE():
                return 'Выкуп посылки у отправителя, стоимость выкупа будет компенсирована с баланса клиента';
            case self::FINANCE_TYPE_REDEEM_RECIPIENT():
                return 'Выкуп посылки у отправителя, стоимость выкупа компенсирует получатель';
            case self::FINANCE_TYPE_COD_BALANCE():
                return 'Наложенный платеж: доставить сумму обратно отправителю';
            case self::FINANCE_TYPE_COD_CARD():
                return 'Наложенный платеж: перевод суммы на привязанную карту клиента';
            case self::FINANCE_TYPE_COD_TOACCOUNT():
                return 'Наложенный платеж: на расчетный счет';
            default:
                throw new \UnexpectedValueException();
        }
    }
}