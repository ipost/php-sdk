<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self STATUS_NONE()     Новое назначение / Курьер еще не приступил к этому назначению
 * @method static self STATUS_ONWAY()    Курьер в пути к точке назначения
 * @method static self STATUS_WAIT()     Курьер ожидает отправителя / получателя
 * @method static self STATUS_DONE()     Назначение завершено
 * @method static self STATUS_CANCELED() Назначение отменено (отмена клиентом, отказ, отправитель/получатель не вышел на связь)
 */
class OrderTargetStatusEnum extends Enum
{
    private const STATUS_NONE = 0;
    private const STATUS_ONWAY = 1;
    private const STATUS_WAIT = 2;
    private const STATUS_DONE = 3;
    private const STATUS_CANCELED = 4;

    public function label(): string
    {
        switch ($this) {
            case self::STATUS_NONE():
                return 'Новое назначение';
            case self::STATUS_ONWAY():
                return 'Курьер в пути к точке назначения';
            case self::STATUS_WAIT():
                return 'Курьер ожидает отправителя / получателя';
            case self::STATUS_DONE():
                return 'Назначение завершено';
            case self::STATUS_CANCELED():
                return 'Назначение отменено';
            default:
                throw new \UnexpectedValueException();
        }
    }
}