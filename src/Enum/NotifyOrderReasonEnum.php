<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self CANCELED_BY_CLIENT()   Клиент хочет отменить заказ
 * @method static self CANCELED_BY_COURIER()  Курьер хочет отменить заказ
 * @method static self TARGET_CANCELED()      Один из пунктов доставки отменен (точная причина отмены пункта - в заказе)
 * @method static self MANUAL_REVIEW()        Заказ на ручной проверке
 * @method static self FRAUD()                Подозрение на мошенничество
 * @method static self COURIER_NOT_ASSIGNED() Курьер не назначен
 * @method static self PAYMENT_FAILED()       Неудачная оплата с карты
 */
class NotifyOrderReasonEnum extends Enum
{
    private const CANCELED_BY_CLIENT = 'CANCELED_BY_CLIENT';
    private const CANCELED_BY_COURIER = 'CANCELED_BY_COURIER';
    private const TARGET_CANCELED = 'TARGET_CANCELED';
    private const MANUAL_REVIEW = 'MANUAL_REVIEW';
    private const FRAUD = 'FRAUD';
    private const COURIER_NOT_ASSIGNED = 'COURIER_NOT_ASSIGNED';
    private const PAYMENT_FAILED = 'PAYMENT_FAILED';

    public function label(): string
    {
        switch ($this) {
            case self::CANCELED_BY_CLIENT():
                return 'Клиент хочет отменить заказ';
            case self::CANCELED_BY_COURIER():
                return 'Курьер хочет отменить заказ';
            case self::TARGET_CANCELED():
                return 'Один из пунктов доставки отменен';
            case self::MANUAL_REVIEW():
                return 'Заказ на ручной проверке';
            case self::FRAUD():
                return 'Подозрение на мошенничество';
            case self::COURIER_NOT_ASSIGNED():
                return 'Курьер не назначен';
            case self::PAYMENT_FAILED():
                return 'Неудачная оплата с карты';
            default:
                throw new \UnexpectedValueException();
        }
    }

    public function origin(): OrderReasonEnum
    {
        return OrderReasonEnum::{'REASON_' . $this->getValue()}();
    }
}