<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self PAYMENT_TYPE_BALANCE()                     Оплата с баланса отправителем
 * @method static self PAYMENT_TYPE_SENDER()                      Оплата наличными отправителем
 * @method static self PAYMENT_TYPE_RECIPIENT()                   Оплата наличными получателем
 * @method static self PAYMENT_TYPE_SPLIT_BALANCE_AND_SENDER()    Разделение оплаты: С баланса отправителем и наличными отправителем
 * @method static self PAYMENT_TYPE_SPLIT_BALANCE_AND_RECIPIENT() Разделение оплаты: С баланса отправителем и наличными получателем
 * @method static self PAYMENT_TYPE_SPLIT_SENDER_AND_RECIPIENT()  Разделение оплаты: Наличными отправителем и получателем
 * @method static self PAYMENT_TYPE_CARD()                        Оплата картой
 * @method static self PAYMENT_TYPE_SPLIT_CARD_AND_SENDER()       Разделение оплаты: с карты и отправителем
 * @method static self PAYMENT_TYPE_SPLIT_CARD_AND_RECIPIENT()    Разделение оплаты: с карты и получателем
 */
class FormOrderPaymentTypeEnum extends Enum
{
    private const PAYMENT_TYPE_BALANCE = 1;
    private const PAYMENT_TYPE_SENDER = 2;
    private const PAYMENT_TYPE_RECIPIENT = 3;
    private const PAYMENT_TYPE_SPLIT_BALANCE_AND_SENDER = 4;
    private const PAYMENT_TYPE_SPLIT_BALANCE_AND_RECIPIENT = 5;
    private const PAYMENT_TYPE_SPLIT_SENDER_AND_RECIPIENT = 6;
    private const PAYMENT_TYPE_CARD = 7;
    private const PAYMENT_TYPE_SPLIT_CARD_AND_SENDER = 8;
    private const PAYMENT_TYPE_SPLIT_CARD_AND_RECIPIENT = 9;

    public function label(): string
    {
        switch ($this) {
            case self::PAYMENT_TYPE_BALANCE():
                return 'Оплата с баланса отправителем';
            case self::PAYMENT_TYPE_SENDER():
                return 'Оплата наличными отправителем';
            case self::PAYMENT_TYPE_RECIPIENT():
                return 'Оплата наличными получателем';
            case self::PAYMENT_TYPE_SPLIT_BALANCE_AND_SENDER():
                return 'С баланса отправителем и наличными отправителем';
            case self::PAYMENT_TYPE_SPLIT_BALANCE_AND_RECIPIENT():
                return 'С баланса отправителем и наличными получателем';
            case self::PAYMENT_TYPE_SPLIT_SENDER_AND_RECIPIENT():
                return 'Наличными отправителем и получателем';
            case self::PAYMENT_TYPE_CARD():
                return 'Оплата картой';
            case self::PAYMENT_TYPE_SPLIT_CARD_AND_SENDER():
                return 'С карты и отправителем';
            case self::PAYMENT_TYPE_SPLIT_CARD_AND_RECIPIENT():
                return 'С карты и получателем';
            default:
                throw new \UnexpectedValueException();
        }
    }
}