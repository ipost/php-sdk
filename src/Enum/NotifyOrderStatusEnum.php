<?php

declare(strict_types=1);

namespace Ipost\SDK\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self INWORK()    Заказ принят в работу
 * @method static self MODERATE()  Заказ находится на модерации (обычно с причиной)
 * @method static self DONE()      Заказ успешно завершен
 * @method static self UNAPPLIED() Не найдено курьера
 * @method static self CANCELED()  Заказ отменен
 */
class NotifyOrderStatusEnum extends Enum
{
    private const INWORK = 'INWORK';
    private const MODERATE = 'MODERATE';
    private const DONE = 'DONE';
    private const UNAPPLIED = 'UNAPPLIED';
    private const CANCELED = 'CANCELED';

    public function label(): string
    {
        switch ($this) {
            case self::INWORK():
                return 'Заказ принят в работу';
            case self::MODERATE():
                return 'Заказ находится на модерации';
            case self::DONE():
                return 'Заказ успешно завершен';
            case self::UNAPPLIED():
                return 'Не найдено курьера';
            case self::CANCELED():
                return 'Заказ отменен';
            default:
                throw new \UnexpectedValueException();
        }
    }

    public function origin(): OrderStatusEnum
    {
        return OrderStatusEnum::{'STATUS_' . $this->getValue()}();
    }
}