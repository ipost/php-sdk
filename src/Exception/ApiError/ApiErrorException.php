<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Exception;
use Ipost\SDK\Exception\IpostException;

class ApiErrorException extends Exception implements IpostException
{
}