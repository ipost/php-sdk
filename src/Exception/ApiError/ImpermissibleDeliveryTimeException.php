<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Exception\LogicException;

class ImpermissibleDeliveryTimeException extends LogicException
{
    private int $min;

    public function __construct(string $message, int $code, int $min)
    {
        parent::__construct($message, $code);

        $this->min = $min;
    }

    public function getMin(): int
    {
        return $this->min;
    }
}