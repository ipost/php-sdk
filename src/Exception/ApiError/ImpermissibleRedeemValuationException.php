<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Exception\LogicException;

class ImpermissibleRedeemValuationException extends LogicException
{
    private float $max;

    public function __construct(string $message, int $code, float $max)
    {
        parent::__construct($message, $code);

        $this->max = $max;
    }

    public function getMax(): float
    {
        return $this->max;
    }
}