<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Exception\LogicException;

class NoMatchingTariffFoundException extends LogicException
{
    private int $maxLength;
    private int $maxWidth;
    private int $maxHeight;
    private int $maxWeight;

    public function __construct(string $message, int $code, int $maxLength, int $maxWidth, int $maxHeight, int $maxWeight)
    {
        parent::__construct($message, $code);

        $this->maxLength = $maxLength;
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;
        $this->maxWeight = $maxWeight;
    }

    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    public function getMaxWidth(): int
    {
        return $this->maxWidth;
    }

    public function getMaxHeight(): int
    {
        return $this->maxHeight;
    }

    public function getMaxWeight(): int
    {
        return $this->maxWeight;
    }
}