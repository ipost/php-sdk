<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Entity\FieldError;
use Ipost\SDK\Exception\LogicException;

class ValidationException extends LogicException
{
    /**
     * @var FieldError[]
     */
    private array $errors;

    public function __construct($message, $code, array $errors)
    {
        parent::__construct($message, $code);

        $this->errors = $errors;
    }

    /**
     * @return FieldError[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}