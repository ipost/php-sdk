<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Exception\LogicException;

class AddressOutOfMapBoundsException extends LogicException
{
    private float $lat;
    private float $long;
    private ?string $reason;

    public function __construct(string $message, int $code, float $lat, float $long, ?string $reason = null)
    {
        parent::__construct($message, $code);

        $this->lat = $lat;
        $this->long = $long;
        $this->reason = $reason;
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function getLong(): float
    {
        return $this->long;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }
}