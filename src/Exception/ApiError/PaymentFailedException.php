<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Exception\LogicException;

class PaymentFailedException extends LogicException
{
}