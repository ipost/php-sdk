<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception\ApiError;

use Ipost\SDK\Exception\LogicException;

class NotEnoughBalanceException extends LogicException
{
    private float $balance;

    public function __construct(string $message, int $code, float $balance)
    {
        parent::__construct($message, $code);

        $this->balance = $balance;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }
}