<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception;

use Exception;

class InvalidSignatureException extends Exception implements IpostException
{
}