<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements IpostException
{
}