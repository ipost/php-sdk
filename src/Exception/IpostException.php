<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception;

use Throwable;

interface IpostException extends Throwable
{
}