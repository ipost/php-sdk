<?php

declare(strict_types=1);

namespace Ipost\SDK\Exception;

use Ipost\SDK\Exception\ApiError\ApiErrorException;

/**
 * @deprecated
 *
 * Use ApiErrorException instead of LogicException
 */
class LogicException extends ApiErrorException implements IpostException
{
}