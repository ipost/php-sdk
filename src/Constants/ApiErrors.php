<?php

namespace Ipost\SDK\Constants;

class ApiErrors
{
    const COMMON_ERROR = 0; // Общая ошибка
    const API_VERSION_CLOSED_ERROR = 1; // Версия API закрыта.
    const NOT_FOUND_ERROR = 44; // Запрашиваемая сущность не известна.
    const VALIDATION_ERROR = 100; // Ошибка валидации сущности.
    const NOT_ENOUGH_BALANCE_ERROR = 104; // Недостаточно средств на балансе
    const BALANCE_NEGATIVE_ERROR = 105; // Баланс меньше нуля
    const NOT_ALLOWED_BY_CONDITION_ERROR = 106; // Действие не разрешено по условию
    const NO_MATCHING_TARIFF_FOUND_ERROR = 109; // Не найден тариф для заказа. Возможно, слишком большой размер посылки/вес груза.
    const IMPERMISSIBLE_VALUATION_ERROR = 114; // Недопустимая оценочная стоимость
    const ORDER_ALREADY_INWORK_ERROR = 116; // Статус заказа был изменен, order.status != STATUS_NEW
    const ADDRESS_OUT_OF_MAP_BOUNDS_ERROR = 117; // Адрес находится за допустимыми пределами
    const PROPERTY_IS_READONLY_ERROR = 118; // Нельзя изменить свойство по каким-либо причинам
    const ADDRESSES_MATCH_ERROR = 119; // Адрес отправителя и адрес получателя не соответствуют условиям (в одном населенном пункте или разных)
    const IMPERMISSIBLE_REDEEM_VALUATION_ERROR = 123; // Недопустимая сумма выкупа посылки
    const IMPERMISSIBLE_DELIVERY_TIME_ERROR = 124; // Время доставки слишком малое для этого маршрута
    const IMPERMISSIBLE_REDELIVERY_TIME_ERROR = 125; // Время обратной доставки слишком малое для этого маршрута
    const IMPERMISSIBLE_CODTOACCOUNT_VALUATION_ERROR = 128; // Недопустимая сумма наложенного платежа на расчетный счет
    const IMPERMISSIBLE_AGE_CHECK_DOB_ERROR = 130; // Указанная дата рождения получателя не соответствует совершеннолетию на дату доставки.
    const COMMENT_REQUIRED_ERROR = 131; // Требуется добавить комментарий
    const OPTION_DISABLED_ERROR = 132; // Опция не доступна
    const VOUCHER_NOT_FOUND_ERROR = 135; // Ваучер не найден
    const CANNOT_PAY_WITH_BALANCE_WHEN_VOUCHER_USED_ERROR = 136; // Нельзя произвести оплату заказа с баланса при использовании ваучера
    const PAYMENT_CARD_NOT_FOUND_ERROR = 139; // Платежная карта не найдена
    const PAYMENT_FAILED_ERROR = 140; // Неуспешный платеж
}