<?php

declare(strict_types=1);

namespace Ipost\SDK\Constants;

class DeliveryTime
{
    const TIME_1800 = 1800;
    const TIME_3600 = 3600;
    const TIME_5400 = 5400;
    const TIME_7200 = 7200;
    const TIME_9000 = 9000;
    const TIME_10800 = 10800;
}