<?php

declare(strict_types=1);

namespace Ipost\SDK;

use GuzzleHttp\Psr7\ServerRequest;
use Ipost\SDK\Entity\Notification;
use Ipost\SDK\Exception\InvalidSignatureException;
use Ipost\SDK\Exception\NotifyException;
use Ipost\SDK\Utils\Hydrator;
use Ipost\SDK\Utils\Json;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class SDKNotify
{
    const TYPE_ORDER_STATUS_UPDATE = 'order:status-update';

    private string $accessToken;
    private ServerRequestInterface $serverRequest;
    private Hydrator $hydrator;
    private LoggerInterface $logger;

    public function __construct(
        string $accessToken,
        ?LoggerInterface $logger = null
    ) {
        $this->accessToken = $accessToken;
        $this->logger = $logger ?? new NullLogger();
        $this->hydrator = new Hydrator();
        $this->serverRequest = ServerRequest::fromGlobals();
    }

    /**
     * @throws NotifyException
     * @throws InvalidSignatureException
     */
    public function getOrderStatusUpdated(): Notification
    {
        if (empty($this->serverRequest->getServerParams()['HTTP_X_SIGNATURE'])
            || $this->serverRequest->getServerParams()['HTTP_X_SIGNATURE'] !== $this->signature())
        {
            $this->logger->error('Notify', ['InvalidSignature']);
            throw new InvalidSignatureException('Access denied', 403);
        }

        $object = $this->getParsed();

        if ($object->type !== self::TYPE_ORDER_STATUS_UPDATE) {
            throw new NotifyException('Unknown notification type', 404);
        }

        return $object;
    }

    private function getParsed(): Notification
    {
        $array = Json::decode($this->serverRequest->getBody()->__toString());

        return $this->hydrator->fromArray(Notification::class, $array);
    }

    private function signature(): string
    {
        return hash_hmac('sha256', $this->serverRequest->getBody()->getContents(), $this->accessToken);
    }
}