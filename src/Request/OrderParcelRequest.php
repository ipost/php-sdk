<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Exception\InvalidArgumentException;

class OrderParcelRequest implements RequestInterface
{
    /**
     * Описание посылки
     */
    private string $description;

    /**
     * Объявленная стоимость посылки
     */
    private int $valuation;

    /**
     * Вес посылки, в граммах
     */
    private int $weight;

    /**
     * Количество мест
     */
    private int $places;

    public function __construct(
        string $description,
        int $valuation,
        int $weight,
        int $places
    ) {
        if (!preg_match('/^.{1,400}$/u', $description)) {
            throw new InvalidArgumentException('Description must be no more than 400 characters.');
        }

        $this->description = $description;
        $this->valuation = $valuation;
        $this->weight = $weight;
        $this->places = $places;
    }

    public function jsonSerialize(): array
    {
        return [
            'description' => $this->description,
            'valuation' => $this->valuation,
            'weight' => $this->weight,
            'places' => $this->places,
        ];
    }
}