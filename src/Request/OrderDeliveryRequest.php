<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Exception\InvalidArgumentException;
use Ipost\SDK\Enum\FormOrderDeliveryFinanceTypeEnum;
use Ipost\SDK\Enum\FormOrderDeliveryRedeliveryTypeEnum;
use Ipost\SDK\Enum\FormOrderTargetPaymentTypeEnum;

class OrderDeliveryRequest implements RequestInterface
{
    /**
     * Имя и фамилия получателя
     */
    private string $name;

    /**
     * Номер телефона получателя без “+”
     */
    private string $phone;

    /**
     * Время прибытия курьера - С
     */
    private ?int $time_from;

    /**
     * Время прибытия курьера - До
     */
    private ?int $time_to;

    /**
     * Способ оплаты
     */
    private ?FormOrderTargetPaymentTypeEnum $payment_type;

    /**
     * Дополнительные финансовые операции
     * Для доставки по межгороду вариант FINANCE_TYPE_COD_REDELIVERY недоступен!
     */
    private ?FormOrderDeliveryFinanceTypeEnum $finance_type;

    /**
     * Тип обратной доставки
     */
    private ?FormOrderDeliveryRedeliveryTypeEnum $redelivery_type;

    /**
     * Требуется ли проверка возраста получателя (совершеннолетие) курьером
     */
    private ?bool $age_check_required;

    /**
     * Дата рождения получателя.
     *
     * Формат YYYY-MM-DD. Активно только вместе с age_check_required=1.
     * Если указано, система даст завершить доставку только если дата рождения получателя,
     * которую проверяет курьер, соответствует указанному в этом поле. Если значение не указано,
     * будет произведена проверка на соотвутствие даты рождения совершеннолетию
     */
    private ?string $age_check_dob;

    /**
     * Требуется ли сверка документов получателя (ИНН).
     *
     * Если указано, то поле personal_id_check_value становится обязательным;
     * Курьер будет проверять ИНН у получателя, вводить в приложении,
     * хеш из поля personal_id_check_value будет сверяться со ИНН получателя.
     * Система не даст завершить заказ, если значения не совпадают.
     */
    private ?bool $personal_id_check_required;

    /**
     * Хеш документов (ИНН) для сверки. Будет проигнорировано, если не установлено поле personal_id_check_required.
     *
     * Значение должно состоять из алгоритма, и самого хеша в шестнадцатиричной кодировке (hex) разделенных двоеточием (algo:hash).
     * Изначальная строка для хеш-функции должна состоять исключительно из цифр (0-9).
     */
    private ?string $personal_id_check_value;

    /**
     * ID привязанной карты пользователя для наложенного платежа
     */
    private ?int $cod_card_id;

    /**
     * Внутренний номер заказа
     */
    private ?string $order_number;

    /**
     * Комментарий заказчика касательно прибытия курьера к получателю, доставки посыло
     */
    private ?string $comment;

    /**
     * Адрес получателя
     */
    private OrderAddressRequest $address;

    /**
     * Посылки (список/массив объектов)
     *
     * @var OrderParcelRequest[]
     */
    private array $parcels;

    public function __construct(
        string                               $name,
        string                               $phone,
        OrderAddressRequest                  $address,
        array                                $parcels,
        ?string                              $order_number = null,
        ?string                              $comment = null,
        ?int                                 $time_from = null,
        ?int                                 $time_to = null,
        ?FormOrderDeliveryFinanceTypeEnum    $finance_type = null,
        ?FormOrderDeliveryRedeliveryTypeEnum $redelivery_type = null,
        ?FormOrderTargetPaymentTypeEnum      $payment_type = null,
        ?bool                                $age_check_required = null,
        ?string                              $age_check_dob = null,
        ?bool                                $personal_id_check_required = null,
        ?string                              $personal_id_check_value = null,
        ?int                                 $cod_card_id = null
    ) {
        if (!preg_match('/^.{1,120}$/u', $name)) {
            throw new InvalidArgumentException('Name must be no more than 120 characters.');
        }

        if (!preg_match('/^\d{12}$/', $phone)) {
            throw new InvalidArgumentException('Phone must be in international format and contain only numbers.');
        }

        if ($age_check_dob && !preg_match('/^\d{4}-\d{2}-\d{2}$/', $age_check_dob)) {
            throw new InvalidArgumentException('Date must be in the format YYYY-MM-DD.');
        }

        if ($personal_id_check_value && !preg_match('/^sha256:[0-9a-f]{64}$/', $personal_id_check_value)) {
            throw new InvalidArgumentException('Hash must be in format sha256:[0-9a-f]{64}.');
        }

        if ($order_number && !preg_match('/^[\x21-\x7E]{1,25}$/', $order_number)) {
            throw new InvalidArgumentException('Order number contains invalid characters.');
        }

        if ($comment && !preg_match('/^.{1,4000}$/su', $comment)) {
            throw new InvalidArgumentException('Comment must be no more than 4000 characters.');
        }

        $this->name = $name;
        $this->phone = $phone;
        $this->time_from = $time_from;
        $this->time_to = $time_to;
        $this->payment_type = $payment_type;
        $this->finance_type = $finance_type;
        $this->redelivery_type = $redelivery_type;
        $this->age_check_required = $age_check_required;
        $this->age_check_dob = $age_check_dob;
        $this->personal_id_check_required = $personal_id_check_required;
        $this->personal_id_check_value = $personal_id_check_value;
        $this->cod_card_id = $cod_card_id;
        $this->order_number = $order_number;
        $this->comment = $comment;
        $this->address = $address;
        $this->parcels = $parcels;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'name' => $this->name,
            'phone' => $this->phone,
            'time_from' => $this->time_from,
            'time_to' => $this->time_to,
            'payment_type' => $this->payment_type,
            'finance_type' => $this->finance_type,
            'redelivery_type' => $this->redelivery_type,
            'age_check_required' => $this->age_check_required,
            'age_check_dob' => $this->age_check_dob,
            'personal_id_check_required' => $this->personal_id_check_required,
            'personal_id_check_value' => $this->personal_id_check_value,
            'cod_card_id' => $this->cod_card_id,
            'order_number' => $this->order_number,
            'comment' => $this->comment,
            'address' => $this->address,
            'parcels' => $this->parcels,
        ], function($v) { return !is_null($v); });
    }
}