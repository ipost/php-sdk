<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

class UpdateCustomPriceRequest implements RequestInterface
{
    private float $custom_price;

    public function __construct(float $custom_price)
    {
        $this->custom_price = $custom_price;
    }

    public function jsonSerialize(): array
    {
        return [
            'custom_price' => $this->custom_price,
        ];
    }
}