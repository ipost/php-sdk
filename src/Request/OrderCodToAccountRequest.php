<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Exception\InvalidArgumentException;

class OrderCodToAccountRequest implements RequestInterface
{
    /**
     * Назначение наложенного платежа
     */
    private string $destination;

    public function __construct(string $destination)
    {
        if (!preg_match('/^.{10,160}$/u', $destination)) {
            throw new InvalidArgumentException('Destination must be at least 10 and no more than 160 characters.');
        }

        $this->destination = $destination;
    }

    public function jsonSerialize(): array
    {
        return [
            'destination' => $this->destination,
        ];
    }
}