<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

class OrderPaymentSplitRequest implements RequestInterface
{
    /**
     * Сумма оплаты с баланса
     */
    private ?int $balance;

    /**
     * Сумма оплаты отправителем
     */
    private ?int $sender;

    /**
     * Сумма оплаты получателем
     */
    private ?int $recipient;

    public function __construct(
        ?int $balance = null,
        ?int $sender = null,
        ?int $recipient = null
    ) {
        $this->balance = $balance;
        $this->sender = $sender;
        $this->recipient = $recipient;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'balance' => $this->balance,
            'sender' => $this->sender,
            'recipient' => $this->recipient,
        ], function($v) { return !is_null($v); });
    }
}