<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

class ReviewRequest implements RequestInterface
{
    /**
     * Оценка заказа
     */
    private int $rating;

    /**
     * Текст отзыва
     */
    private ?string $text;

    public function __construct(int $rating, ?string $text = null)
    {
        $this->rating = $rating;
        $this->text = $text;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'rating' => $this->rating,
            'text' => $this->text,
        ], function($v) { return !is_null($v); });
    }
}