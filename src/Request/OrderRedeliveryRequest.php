<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Enum\FormOrderTargetPaymentTypeEnum;

class OrderRedeliveryRequest implements RequestInterface
{
    /**
     * Время прибытия курьера - С
     */
    private ?int $time_from;

    /**
     * Время прибытия курьера - До
     */
    private ?int $time_to;

    /**
     * Способ оплаты
     */
    private ?FormOrderTargetPaymentTypeEnum $payment_type;

    public function __construct(
        ?int $time_from = null,
        ?int $time_to = null,
        ?FormOrderTargetPaymentTypeEnum $payment_type = null
    ) {
        $this->time_from = $time_from;
        $this->time_to = $time_to;
        $this->payment_type = $payment_type;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'time_from' => $this->time_from,
            'time_to' => $this->time_to,
            'payment_type' => $this->payment_type,
        ], function($v) { return !is_null($v); });
    }
}