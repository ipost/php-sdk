<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Exception\InvalidArgumentException;

class OrderAddressRequest implements RequestInterface
{
    /**
     * Гео-координаты - latitude
     */
    private float $lat;

    /**
     * Гео-координаты - longitude
     */
    private float $long;

    /**
     * Название города/населенного пункта
     */
    private string $city;

    /**
     * Название улицы (должно начинаться с “ул.”, “пр.”, и т.п.)
     */
    private ?string $street;

    /**
     * Номер дома/здания
     */
    private ?string $number;

    /**
     * Номер квартиры/офиса
     */
    private ?string $flat;

    public function __construct(
        float $lat,
        float $long,
        string $city,
        string $street,
        ?string $number = null,
        ?string $flat = null
    ) {
        if (!preg_match('/^.{1,120}$/u', $city)) {
            throw new InvalidArgumentException('City must be no more than 120 characters.');
        }

        if (!preg_match('/^.{1,120}$/u', $street)) {
            throw new InvalidArgumentException('Street must be no more than 120 characters.');
        }

        if ($number && !preg_match('/^.{1,20}$/u', $number)) {
            throw new InvalidArgumentException('Number must be no more than 20 characters.');
        }

        if ($flat && !preg_match('/^.{1,20}$/u', $flat)) {
            throw new InvalidArgumentException('Flat must be no more than 20 characters.');
        }

        $this->lat = $lat;
        $this->long = $long;
        $this->city = $city;
        $this->street = $street;
        $this->number = $number;
        $this->flat = $flat;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'lat' => $this->lat,
            'long' => $this->long,
            'city' => $this->city,
            'street' => $this->street,
            'number' => $this->number,
            'flat' => $this->flat,
        ], function($v) { return !is_null($v); });
    }
}