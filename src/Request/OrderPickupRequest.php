<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Exception\InvalidArgumentException;

class OrderPickupRequest implements RequestInterface
{
    /**
     * Время прибытия курьера - С
     */
    private ?int $time_from;

    /**
     * Время прибытия курьера - До
     */
    private ?int $time_to;

    /**
     * Комментарий заказчика касательно прибытия курьера к отправителю, забора посылок
     */
    private ?string $comment;

    public function __construct(
        ?int $time_from = null,
        ?int $time_to = null,
        ?string $comment = null
    ) {
        if ($comment && !preg_match('/^.{1,4000}$/su', $comment)) {
            throw new InvalidArgumentException('Comment must be no more than 4000 characters.');
        }

        $this->time_from = $time_from;
        $this->time_to = $time_to;
        $this->comment = $comment;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'time_from' => $this->time_from,
            'time_to' => $this->time_to,
            'comment' => $this->comment,
        ], function($v) { return !is_null($v); });
    }
}