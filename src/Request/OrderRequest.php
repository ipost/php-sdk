<?php

declare(strict_types=1);

namespace Ipost\SDK\Request;

use Ipost\SDK\Enum\FormOrderPaymentTypeEnum;
use Ipost\SDK\Exception\InvalidArgumentException;

class OrderRequest implements RequestInterface
{
    /**
     * Время доставки в секундах
     */
    private ?int $delivery_time;

    /**
     * Краткое описание заказа
     */
    private ?string $description;

    /**
     * Габариты отправления: длина, мм
     */
    private int $length;

    /**
     * Габариты отправления: ширина, мм
     */
    private int $width;

    /**
     * Габариты отправления: высота, мм
     */
    private int $height;

    /**
     * Имя и фамилия отправителя
     */
    private ?string $name;

    /**
     * Номер телефона отправителя
     */
    private ?string $phone;

    /**
     * Способ оплаты
     */
    private ?FormOrderPaymentTypeEnum $payment_type;

    /**
     * Настройка разделения оплаты
     */
    private ?OrderPaymentSplitRequest $payment_split;

    /**
     * Добавочная стоимость доставки
     */
    private ?float $custom_price;

    /**
     * Адрес отправителя
     */
    private OrderAddressRequest $address;

    /**
     * Информация о заборе посылки
     */
    private OrderPickupRequest $pickup;

    /**
     * @var OrderDeliveryRequest[] Информация о доставках NB: список/массив объектов
     */
    private array $deliveries;

    /**
     * Информация об обратной доставке
     */
    private ?OrderRedeliveryRequest $redelivery;

    /**
     * Информация об наложенном платеже на расчетный счет
     */
    private ?OrderCodToAccountRequest $codtoaccount;

    /**
     * Код ваучера для скидки
     */
    private ?string $voucher_code;

    /**
     * Урл, на который сервер будет присылать уведомления об изменении статуса заказа.
     * Значение должно быть валидным урлом. IDN домены не принимаются.
     */
    private ?string $notify_url;

    /**
     * ID платежной карты
     */
    public ?int $payment_card_id;

    public function __construct(
        int                       $length,
        int                       $width,
        int                       $height,
        array                     $deliveries,
        OrderAddressRequest       $address,
        OrderPickupRequest        $pickup,
        ?FormOrderPaymentTypeEnum $payment_type = null,
        ?int                      $delivery_time = null,
        ?string                   $description = null,
        ?string                   $name = null,
        ?string                   $phone = null,
        ?OrderPaymentSplitRequest $payment_split = null,
        ?OrderRedeliveryRequest   $redelivery = null,
        ?OrderCodToAccountRequest $codtoaccount = null,
        ?float                    $custom_price = null,
        ?string                   $voucher_code = null,
        ?string                   $notify_url = null,
        ?int                      $payment_card_id = null
    ) {
        if ($description && !preg_match('/^.{1,400}$/u', $description)) {
            throw new InvalidArgumentException('Description must be no more than 400 characters.');
        }

        if ($name && !preg_match('/^.{1,120}$/u', $name)) {
            throw new InvalidArgumentException('Name must be no more than 120 characters.');
        }

        if ($phone && !preg_match('/^\d{12}$/', $phone)) {
            throw new InvalidArgumentException('Phone must be in international format and contain only numbers.');
        }

        if ($voucher_code && !preg_match('/^[A-Z0-9]+$/', $voucher_code)) {
            throw new InvalidArgumentException('Voucher Code must only contain uppercase letters and numbers.');
        }

        if ($notify_url && !preg_match('/^.{1,1024}$/u', $notify_url)) {
            throw new InvalidArgumentException('Notify url must be no more than 1024 characters.');
        }

        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->address = $address;
        $this->pickup = $pickup;
        $this->deliveries = $deliveries;
        $this->payment_type = $payment_type;
        $this->delivery_time = $delivery_time;
        $this->description = $description;
        $this->name = $name;
        $this->phone = $phone;
        $this->payment_split = $payment_split;
        $this->redelivery = $redelivery;
        $this->codtoaccount = $codtoaccount;
        $this->custom_price = $custom_price;
        $this->voucher_code = $voucher_code;
        $this->notify_url = $notify_url;
        $this->payment_card_id = $payment_card_id;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'length' => $this->length,
            'width' => $this->width,
            'height' => $this->height,
            'address' => $this->address,
            'pickup' => $this->pickup,
            'deliveries' => $this->deliveries,
            'payment_type' => $this->payment_type,
            'delivery_time' => $this->delivery_time,
            'description' => $this->description,
            'name' => $this->name,
            'phone' => $this->phone,
            'payment_split' => $this->payment_split,
            'redelivery' => $this->redelivery,
            'codtoaccount' => $this->codtoaccount,
            'custom_price' => $this->custom_price,
            'voucher_code' => $this->voucher_code,
            'notify_url' => $this->notify_url,
            'payment_card_id' => $this->payment_card_id,
        ], function($v) { return !is_null($v); });
    }
}