<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

class ErrorResponse
{
    public int $code;

    public string $message;

    /**
     * @var \Ipost\SDK\Entity\FieldError[]|null
     */
    public ?array $errors = null;

    /**
     * Текущая сумма на балансе пользователя
     */
    public ?float $balance = null;

    /**
     * Максимальная длина (мм) отправления
     */
    public ?int $maxLength = null;

    /**
     * Максимальная ширина (мм) отправления
     */
    public ?int $maxWidth = null;

    /**
     * Максимальная высота (мм) отправления
     */
    public ?int $maxHeight = null;

    /**
     * Максимальный вес (г) отправления
     */
    public ?int $maxWeight = null;

    /**
     * Текстовое описание ошибки, которое можно показать пользователю.
     */
    public ?string $reason = null;

    /**
     * Гео-координаты адреса - latitude
     */
    public ?float $lat = null;

    /**
     * Гео-координаты адреса - longitude
     */
    public ?float $long = null;

    /**
     * Максимальное допустимая сумма
     */
    public ?float $max = null;

    /**
     * Минимальное допустимое время
     */
    public ?int $min = null;
}