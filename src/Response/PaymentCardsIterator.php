<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Countable;
use Ipost\SDK\Entity\PaymentCard;
use Iterator;

class PaymentCardsIterator implements Iterator, Countable
{
    /**
     * @var PaymentCard[]
     */
    private array $collection;

    public function __construct(array $collection)
    {
        $this->collection = $collection;
    }

    public function current()
    {
        return current($this->collection);
    }

    public function next()
    {
        return next($this->collection);
    }

    public function key()
    {
        return key($this->collection);
    }

    public function valid(): bool
    {
        return key($this->collection) !== null;
    }

    public function rewind()
    {
        return reset($this->collection);
    }

    public function count(): int
    {
        return count($this->collection);
    }
}