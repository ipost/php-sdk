<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Ipost\SDK\Entity\NavMeta;

class CourierReviewsResponse
{
    /**
     * @var \Ipost\SDK\Entity\CourierReview[]
     */
    public array $items;

    public NavMeta $meta;
}