<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

class PaymentCardsResponse
{
    /**
     * @var \Ipost\SDK\Entity\PaymentCard[]
     */
    public array $items;
}