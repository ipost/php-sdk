<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Ipost\SDK\Entity\PriceParts;

class OrderCreatedResponse
{
    /**
     * ID нового заказа
     */
    public int $id;

    /**
     * Стоимость доставки
     */
    public float $price;

    /**
     * Составные части стоимости заказа
     */
    public PriceParts $price_parts;

    /**
     * Баланс пользователя после запроса
     */
    public float $balance;
}