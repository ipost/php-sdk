<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Ipost\SDK\Entity\NavMeta;

class OrdersResponse
{
    /**
     * @var \Ipost\SDK\Entity\OrderShort[]
     */
    public array $items;

    public NavMeta $meta;
}