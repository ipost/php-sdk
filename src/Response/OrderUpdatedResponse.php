<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

class OrderUpdatedResponse
{
    /**
     * Новая стоимость
     */
    public float $price;

    /**
     * Баланс пользователя после изменения добавочной стоимости
     */
    public float $balance;
}