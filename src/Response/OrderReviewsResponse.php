<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Ipost\SDK\Entity\NavMeta;

class OrderReviewsResponse
{
    /**
     * @var \Ipost\SDK\Entity\Review[]
     */
    public array $items;

    public NavMeta $meta;
}