<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Countable;
use Ipost\SDK\Entity\NavMeta;
use Ipost\SDK\Entity\Review;
use Iterator;

class OrderReviewsIterator implements Iterator, Countable
{
    /**
     * @var Review[]
     */
    private array $collection;

    private NavMeta $meta;

    public function __construct(array $collection, NavMeta $meta)
    {
        $this->collection = $collection;
        $this->meta = $meta;
    }

    /**
     * Кол-во элементов в перечислении
     */
    public function getTotalCount(): int
    {
        return $this->meta->total_count;
    }

    /**
     * Кол-во страниц в навигации по перечислению
     */
    public function getPageCount(): int
    {
        return $this->meta->page_count;
    }

    /**
     * Текущая страница навигации по перечислению
     */
    public function getCurrentPage(): int
    {
        return $this->meta->current_page;
    }

    public function current()
    {
        return current($this->collection);
    }

    public function next()
    {
        return next($this->collection);
    }

    public function key()
    {
        return key($this->collection);
    }

    public function valid(): bool
    {
        return key($this->collection) !== null;
    }

    public function rewind()
    {
        return reset($this->collection);
    }

    public function count(): int
    {
        return count($this->collection);
    }
}