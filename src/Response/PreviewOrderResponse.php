<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

use Ipost\SDK\Entity\PriceParts;
use Ipost\SDK\Enum\OrderTariffTypeEnum;

class PreviewOrderResponse
{
    /**
     * Стоимость
     */
    public ?float $price;

    /**
     * Составные части стоимости заказа
     */
    public ?PriceParts $price_parts;

    /**
     * Сумма залога которая будет заблокирована у клиента (при выкупе посылки курьером с гарантированием)
     */
    public ?float $pledge;

    /**
     * Баланс пользователя после запроса
     */
    public float $balance;

    /**
     * Минимальное время доставки
     */
    public ?int $min_delivery_time;

    /**
     * Тип доставки
     */
    public ?OrderTariffTypeEnum $tariff_type;
}