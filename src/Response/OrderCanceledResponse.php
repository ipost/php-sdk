<?php

declare(strict_types=1);

namespace Ipost\SDK\Response;

class OrderCanceledResponse
{
    /**
     * Размер штрафа
     */
    public float $fine;

    /**
     * Баланс пользователя после отмены заказа
     */
    public float $balance;
}