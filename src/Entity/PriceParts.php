<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class PriceParts
{
    /**
     * Надбавка за наложенный платеж (в случае передачи суммы наложенного платежа на счет iPOST клиента)
     */
    public int $cod;
    
    /**
     * Свободная надбавка клиента
     */
    public int $custom;

    /**
     * Базовая стоимость доставки
     */
    public int $delivery;
    
    /**
     * Надбавка за расстояние
     */
    public int $distance;
    
    /**
     * Надбавка за срочность
     */
    public int $fast;

    /**
     * Комиссия за обслуживание
     */
    public int $fee_service;
    
    /**
     * Надбавка за выходной / праздник
     */
    public int $holiday;
    
    /**
     * Надбавка за нерабочее время
     */
    public int $night;

    /**
     * Надбавка в пиковое время
     */
    public int $peak;
    
    /**
     *  Надбавка на выкуп посылки курьером
     */
    public int $redeem;
    
    /**
     * Стоимость обратной доставки
     */
    public int $redelivery;
    
    /**
     * Надбавка на оценочную стоимость
     */
    public int $valuation;
    
    /**
     * Надбавка за вес
     */
    public int $weight;
}