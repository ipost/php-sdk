<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class FieldError
{
    /**
     * Название поля
     */
    public string $field;

    /**
     * Текстовое описание ошибки
     */
    public string $message;

    /**
     * @var \Ipost\SDK\Entity\FieldError[]|null
     */
    public ?array $errors = null;
}