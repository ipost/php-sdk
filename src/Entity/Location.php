<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class Location
{
    /**
     * Гео-координаты курьера latitude
     */
    public float $lat;

    /**
     * Гео-координаты курьера longitude
     */
    public float $long;
}