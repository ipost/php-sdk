<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class NavMeta
{
    /**
     * Кол-во элементов в перечислении
     */
    public int $total_count;

    /**
     * Кол-во страниц в навигации по перечислению
     */
    public int $page_count;

    /**
     * Текущая страница навигации по перечислению
     */
    public int $current_page;
}