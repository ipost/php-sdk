<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class CourierInfo
{
    /**
     * Уникальный хеш (ID) курьера
     */
    public string $idhash;

    /**
     * Имя и фамилия курьера
     */
    public string $name;

    /**
     * Номер телефона курьера
     */
    public string $phone;

    /**
     * URL аватара курьера
     */
    public ?string $avatar;

    /**
     * Рейтинг курьера
     */
    public float $rating;

    /**
     * Кол-во доставок курьера
     */
    public int $orders_count;

    /**
     * Дата регистрации курьера
     */
    public int $created_at;
}