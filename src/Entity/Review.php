<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class Review
{
    /**
     * ID заказа
     */
    public int $order_id;

    /**
     * ID доставки. Если null, то отзыв оставил клиент, иначе оставил получатель.
     */
    public ?int $target_id;

    /**
     * Рейтинг (от 1 до 5)
     */
    public int $rating;

    /**
     * Текст отзыва
     */
    public ?string $text;

    /**
     * Дата создания отзыва
     */
    public int $created_at;

    /**
     * Дата изменения отзыва
     */
    public int $updated_at;
}