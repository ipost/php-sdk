<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class PaymentCard
{
    /**
     * Id карты
     */
    public int $id;

    /**
     * Тип карты (MASTERCARD, VISA, может какой-то другой вариант)
     */
    public string $type;

    /**
     * Номер карты (частично засекреченный)
     */
    public string $number;
}