<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\CourierReviewAuthorTypeEnum;

class CourierReview
{
    /**
     * Тип пользователя оставившего отзыв
     */
    public CourierReviewAuthorTypeEnum $author_type;

    /**
     * Рейтинг (от 1 до 5)
     */
    public int $rating;

    /**
     * Текст отзыва
     */
    public ?string $text;

    /**
     * Дата создания отзыва
     */
    public int $created_at;

    /**
     * Дата изменения отзыва
     */
    public int $updated_at;
}