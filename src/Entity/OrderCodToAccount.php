<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\OrderCodToAccountStatusEnum;

class OrderCodToAccount
{
    /**
     * Крайний срок зачисления средств на расчетный счет. Значение устанавливается автоматически после доставки посылки
     */
    public ?int $time_to;

    /**
     * Сумма наложенного платежа
     */
    public float $amount;

    /**
     * Назначение платежа
     */
    public string $destination;

    /**
     * Название компании, получателя платежа
     */
    public string $recipient_name;

    /**
     * Код ЕГРПОУ компании, получателя платежа
     */
    public string $recipient_egrpou;

    /**
     * IBAN компании, получателя платежа
     */
    public string $recipient_iban;

    /**
     * Статус наложенного платежа
     */
    public OrderCodToAccountStatusEnum $status;

    /**
     * Дата изменения полей структуры (В частности, status)
     */
    public int $updated_at;
}