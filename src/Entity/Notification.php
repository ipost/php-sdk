<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\NotifyOrderReasonEnum;
use Ipost\SDK\Enum\NotifyOrderStatusEnum;

class Notification
{
    /**
     * Тип уведомления. На данный момент единственное значение - “order:status-update”. Рекомендуется все же проверять это поле, и игнорировать уведомление, если значение отличается от этого.
     */
    public string $type;

    /**
     * ID заказа
     */
    public int $order_id;

    /**
     * Статус заказа
     */
    public ?NotifyOrderStatusEnum $status;

    /**
     * Причина
     */
    public ?NotifyOrderReasonEnum $reason;

    /**
     * Информация о курьере
     */
    public ?CourierInfo $courier;

    /**
     * Текущая геолокация курьера
     */
    public ?Location $courier_location;

    /**
     * ID пересозданного заказа
     */
    public ?int $recreated_id;

    /**
     * Обновление по пункту отправителя
     */
    public ?NotifyPickupUpdate $pickup_update;

    /**
     * Обновления по пунткам получателя
     *
     * @var \Ipost\SDK\Entity\NotifyDeliveryUpdate[]|null Посылки
     */
    public ?array $delivery_updates;

    /**
     * Обновление по пункту обратной доставки
     */
    public ?NotifyRedeliveryUpdate $redelivery_update;

    /**
     * Обновление по наложенному платежу на р/с
     */
    public ?NotifyCodToAccountUpdate $codtoaccount_update;
}