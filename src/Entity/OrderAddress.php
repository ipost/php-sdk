<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class OrderAddress
{
    /**
     * Гео-координаты - latitude
     */
    public float $lat;

    /**
     * Гео-координаты - longitude
     */
    public float $long;

    /**
     * Название города/населенного пункта
     */
    public string $city;

    /**
     * Название улицы (должно начинаться с “ул.”, “пр.”, и т.п.)
     */
    public string $street;

    /**
     * Номер дома/здания
     */
    public ?string $number;

    /**
     * Номер квартиры/офиса
     */
    public ?string $flat;
}