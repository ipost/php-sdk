<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class OrderPaymentSplit
{
    /**
     * Сумма оплаты с баланса
     */
    public ?float $balance;

    /**
     * Сумма оплаты с карты
     */
    public ?float $card;

    /**
     * Сумма оплаты отправителем
     */
    public ?float $sender;

    /**
     * Сумма оплаты получателем
     */
    public ?float $recipient;
}