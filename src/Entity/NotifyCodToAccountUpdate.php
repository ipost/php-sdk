<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\NotifyCodToAccountStatusEnum;

class NotifyCodToAccountUpdate
{
    /**
     * Статус
     */
    public NotifyCodToAccountStatusEnum $status;
}