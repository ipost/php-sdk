<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\OrderTargetReasonEnum;
use Ipost\SDK\Enum\OrderTargetStatusEnum;

class OrderPickup
{
    /**
     * Время прибытия курьера - С
     */
    public ?int $time_from;

    /**
     * Время прибытия курьера - До
     */
    public ?int $time_to;

    /**
     * Комментарий заказчика касательно прибытия курьера к отправителю, забора посылок
     */
    public ?string $comment;

    /**
     * Статус забора посылок
     */
    public OrderTargetStatusEnum $status;

    /**
     * Причина текущего статуса
     */
    public OrderTargetReasonEnum $reason;

    /**
     * Описание причины (только при reason = REASON_CUSTOM)
     */
    public ?string $reason_custom;

    /**
     * Дата изменения полей структуры (В частности, status)
     */
    public int $updated_at;

    /**
     * @var \Ipost\SDK\Entity\Image[] Фотографии посылок
     */
    public array $images;
}