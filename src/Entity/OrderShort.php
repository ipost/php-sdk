<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\OrderPaymentTypeEnum;
use Ipost\SDK\Enum\OrderReasonEnum;
use Ipost\SDK\Enum\OrderStatusEnum;
use Ipost\SDK\Enum\OrderTariffTypeEnum;

class OrderShort
{
    /**
     * ID заказа
     */
    public int $id;

    /**
     * Текущий статус исполнения заказа
     */
    public OrderStatusEnum $status;

    /**
     * Причина текущего статуса
     */
    public OrderReasonEnum $reason;

    /**
     * Тип доставки
     */
    public OrderTariffTypeEnum $tariff_type;

    /**
     * Краткое описание доставляемого
     */
    public string $description;

    /**
     * Суммарная объявленная стоимость посылок
     */
    public float $valuation;

    /**
     * Габариты отправления: длина, мм
     */
    public int $length;

    /**
     * Габариты отправления: ширина, мм
     */
    public int $width;

    /**
     * Габариты отправления: высота, мм
     */
    public int $height;

    /**
     * Суммарный вес посылок
     */
    public int $weight;

    /**
     * Суммарное кол-во мест, занимаемое посылками
     */
    public int $places;

    /**
     * Имя и фамилия отправителя
     */
    public string $name;

    /**
     * Номер телефона отправителя NB: без “+”
     */
    public string $phone;

    /**
     * Стоимость заказа
     */
    public float $price;

    /**
     * Способ оплаты
     */
    public ?OrderPaymentTypeEnum $payment_type;

    /**
     * Разделение оплаты
     */
    public ?OrderPaymentSplit $payment_split;

    /**
     * Надбавка клиента
     */
    public float $custom_price;

    /**
     * Расстояние доставки (в метрах)
     */
    public int $delivery_distance;

    /**
     * Дата изменения заказа
     * Поле updated_at означает дату изменения полей заказа. Это значение не относится ко вложенным структурам.
     */
    public int $updated_at;

    /**
     * Дата создания заказа
     */
    public int $created_at;
}