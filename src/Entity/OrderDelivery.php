<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\OrderDeliveryFinanceTypeEnum;
use Ipost\SDK\Enum\OrderDeliveryRedeliveryTypeEnum;
use Ipost\SDK\Enum\OrderTargetPaymentTypeEnum;
use Ipost\SDK\Enum\OrderTargetReasonEnum;
use Ipost\SDK\Enum\OrderTargetStatusEnum;

class OrderDelivery
{
    /**
     * ID доставки
     */
    public int $id;

    /**
     * Имя и фамилия получателя
     */
    public string $name;

    /**
     * Номер телефона получателя NB: без “+”
     */
    public string $phone;

    /**
     * Время прибытия курьера - С
     */
    public ?int $time_from;

    /**
     * Время прибытия курьера - До
     */
    public ?int $time_to;

    /**
     * Способ оплаты за доставку (актуально только для мультидоставки)
     */
    public ?OrderTargetPaymentTypeEnum $payment_type;

    /**
     * Сумма оплаты за доставку (актуально только для мультидоставки)
     */
    public ?float $payment_amount;

    /**
     * Дополнительные финансовые операции
     */
    public OrderDeliveryFinanceTypeEnum $finance_type;

    /**
     * Тип обратной доставки
     */
    public OrderDeliveryRedeliveryTypeEnum $redelivery_type;

    /**
     * Требуется ли проверка возраста получателя (совершеннолетие)
     */
    public bool $age_check_required;

    /**
     * Точная дата рождения получателя, которую необходимо проверить.
     */
    public ?string $age_check_dob;

    /**
     * Требуется ли сверка документов (ИНН)
     */
    public bool $personal_id_check_required;

    /**
     * Внутренний номер заказа
     */
    public ?string $order_number;

    /**
     * Код подтверждения доставки
     */
    public int $confirm_code;

    /**
     * Комментарий заказчика касательно прибытия курьера к получателю, доставки посылок
     */
    public ?string $comment;

    /**
     * Статус доставки
     */
    public OrderTargetStatusEnum $status;

    /**
     * Причина текущего статуса
     */
    public OrderTargetReasonEnum $reason;

    /**
     * Описание причины (только при reason = REASON_CUSTOM)
     */
    public ?string $reason_custom;

    /**
     * Дата изменения полей структуры (В частности, status)
     */
    public int $updated_at;

    /**
     * Адрес получателя
     */
    public OrderAddress $address;

    /**
     * @var \Ipost\SDK\Entity\OrderParcel[] Посылки
     */
    public array $parcels;

    /**
     * @var \Ipost\SDK\Entity\Image[] Фотографии посылок
     */
    public array $images;
}