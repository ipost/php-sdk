<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\OrderPaymentTypeEnum;
use Ipost\SDK\Enum\OrderReasonEnum;
use Ipost\SDK\Enum\OrderStatusEnum;
use Ipost\SDK\Enum\OrderTariffTypeEnum;

/**
 * Class Order
 */
class Order
{
    /**
     * ID заказа
     */
    public int $id;

    /**
     * ID родительского заказа (только в пересозданных системой заказах)
     */
    public ?int $origin_id;

    /**
     * ID дочернего заказа (только если заказ был пересоздан)
     */
    public ?int $recreated_id;

    /**
     * Текущий статус исполнения заказа OrderStatusEnum
     */
    public OrderStatusEnum $status;

    /**
     * Причина текущего статуса
     */
    public OrderReasonEnum $reason;

    /**
     * Тип доставки
     */
    public OrderTariffTypeEnum $tariff_type;

    /**
     * Время доставки (доставить за X)
     */
    public ?int $delivery_time;

    /**
     * Краткое описание доставляемого
     */
    public string $description;

    /**
     * Суммарная объявленная стоимость посылок
     */
    public float $valuation;

    /**
     * Габариты отправления: длина, мм
     */
    public int $length;

    /**
     * Габариты отправления: ширина, мм
     */
    public int $width;

    /**
     * Габариты отправления: высота, мм
     */
    public int $height;

    /**
     * Суммарный вес посылок
     */
    public int $weight;

    /**
     * Суммарное кол-во мест, занимаемое посылками
     */
    public int $places;

    /**
     * Имя и фамилия отправителя
     */
    public string $name;

    /**
     * Номер телефона отправителя NB: без “+”
     */
    public string $phone;

    /**
     * Стоимость заказа
     */
    public float $price;

    /**
     * Способ оплаты
     */
    public ?OrderPaymentTypeEnum $payment_type;

    /**
     * Разделение оплаты
     */
    public ?OrderPaymentSplit $payment_split;

    /**
     * Надбавка клиента
     */
    public float $custom_price;

    /**
     * Расстояние доставки (в метрах)
     */
    public int $delivery_distance;

    /**
     * Дата изменения заказа
     * Поле updated_at означает дату изменения полей заказа. Это значение не относится ко вложенным структурам.
     */
    public int $updated_at;

    /**
     * Дата создания заказа
     */
    public int $created_at;

    /**
     * Адрес отправителя
     */
    public OrderAddress $address;

    /**
     * Информация о заборе посылки
     */
    public OrderPickup $pickup;

    /**
     * @var \Ipost\SDK\Entity\OrderDelivery[] Информация о доставках
     */
    public array $deliveries;

    /**
     * Информация об обратной доставке
     */
    public ?OrderRedelivery $redelivery;

    /**
     * Информация о наложенном платеже на расчетный счет
     */
    public ?OrderCodToAccount $codtoaccount;

    /**
     * Информация о курьере
     */
    public ?CourierInfo $courier;

    /**
     * Информация об отзывах
     */
    public ReviewMeta $review_meta;

    /**
     * Отзыв клиента
     * @deprecated
     */
    public ?Review $review;
}