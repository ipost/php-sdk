<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class OrderParcel
{
    /**
     * Описание посылки
     */
    public string $description;

    /**
     * Объявленная стоимость посылки
     */
    public string $valuation;

    /**
     * Вес посылки
     */
    public int $weight;

    /**
     * Количество мест
     */
    public int $places;
}