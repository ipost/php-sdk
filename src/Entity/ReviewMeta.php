<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class ReviewMeta
{
    /**
     * Средняя оценка
     */
    public float $avg_rating;

    /**
     * Количество отзывов
     */
    public int $count;
}