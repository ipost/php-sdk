<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

class Image
{
    /**
     * ID изображения
     */
    public int $id;

    /**
     * URL изображения
     */
    public string $url;
}