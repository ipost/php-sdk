<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\OrderTargetPaymentTypeEnum;
use Ipost\SDK\Enum\OrderTargetReasonEnum;
use Ipost\SDK\Enum\OrderTargetStatusEnum;

class OrderRedelivery
{
    /**
     * Время прибытия курьера - С
     */
    public ?int $time_from;

    /**
     * Время прибытия курьера - До
     */
    public ?int $time_to;

    /**
     * Способ оплаты за доставку (актуально только для мультидоставки)
     */
    public ?OrderTargetPaymentTypeEnum $payment_type;

    /**
     * Сумма оплаты за доставку (актуально только для мультидоставки)
     */
    public ?float $payment_amount;

    /**
     * Код подтверждения доставки
     */
    public int $confirm_code;

    /**
     * Статус доставки
     */
    public OrderTargetStatusEnum $status;

    /**
     * Причина текущего статуса
     */
    public OrderTargetReasonEnum $reason;

    /**
     * Описание причины (только при reason = REASON_CUSTOM)
     */
    public ?string $reason_custom;

    /**
     * Дата изменения полей структуры (В частности, status)
     */
    public int $updated_at;

    /**
     * @var \Ipost\SDK\Entity\Image[] Фотографии посылок
     */
    public array $images;
}