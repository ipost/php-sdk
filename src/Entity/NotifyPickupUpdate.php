<?php

declare(strict_types=1);

namespace Ipost\SDK\Entity;

use Ipost\SDK\Enum\NotifyOrderTargetReasonEnum;
use Ipost\SDK\Enum\NotifyOrderTargetStatusEnum;

class NotifyPickupUpdate
{
    /**
     * Статус
     */
    public NotifyOrderTargetStatusEnum $status;

    /**
     * Причина статуса
     * Поле reason присутствует только при наличии причины установки статуса (актуально для “CANCELED”).
     */
    public ?NotifyOrderTargetReasonEnum $reason;

    /**
     * Другая причина статуса
     * Поле reason_custom присутствует только если reason = "CUSTOM".
     */
    public ?string $reason_custom;
}