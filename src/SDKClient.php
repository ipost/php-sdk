<?php

declare(strict_types=1);

namespace Ipost\SDK;

use GuzzleHttp\Psr7\Request;
use Ipost\SDK\Constants\ApiErrors;
use Ipost\SDK\Entity\Location;
use Ipost\SDK\Entity\Order;
use Ipost\SDK\Enum\CourierReviewAuthorTypeEnum;
use Ipost\SDK\Exception\ApiError\AddressesMatchException;
use Ipost\SDK\Exception\ApiError\AddressOutOfMapBoundsException;
use Ipost\SDK\Exception\ApiError\ApiErrorException;
use Ipost\SDK\Exception\ApiError\ApiVersionClosedException;
use Ipost\SDK\Exception\ApiError\BalanceNegativeException;
use Ipost\SDK\Exception\ApiError\CannotPayWithBalanceWhenVoucherUsedException;
use Ipost\SDK\Exception\ApiError\CommentRequiredException;
use Ipost\SDK\Exception\ApiError\CommonException;
use Ipost\SDK\Exception\ApiError\ImpermissibleAgeCheckDobException;
use Ipost\SDK\Exception\ApiError\ImpermissibleCodtoaccountValuationException;
use Ipost\SDK\Exception\ApiError\ImpermissibleDeliveryTimeException;
use Ipost\SDK\Exception\ApiError\ImpermissibleRedeemValuationException;
use Ipost\SDK\Exception\ApiError\ImpermissibleRedeliveryTimeException;
use Ipost\SDK\Exception\ApiError\ImpermissibleValuationException;
use Ipost\SDK\Exception\ApiError\NoMatchingTariffFoundException;
use Ipost\SDK\Exception\ApiError\NotAllowedByConditionException;
use Ipost\SDK\Exception\ApiError\NotEnoughBalanceException;
use Ipost\SDK\Exception\ApiError\NotFoundException;
use Ipost\SDK\Exception\ApiError\OptionDisabledException;
use Ipost\SDK\Exception\ApiError\OrderAlreadyInworkException;
use Ipost\SDK\Exception\ApiError\PaymentCardNotFoundException;
use Ipost\SDK\Exception\ApiError\PaymentFailedException;
use Ipost\SDK\Exception\ApiError\PropertyIsReadonlyException;
use Ipost\SDK\Exception\ApiError\ValidationException;
use Ipost\SDK\Exception\ApiError\VoucherNotFoundException;
use Ipost\SDK\Exception\ResponseException;
use Ipost\SDK\Request\OrderRequest;
use Ipost\SDK\Request\RequestInterface;
use Ipost\SDK\Request\ReviewRequest;
use Ipost\SDK\Request\UpdateCustomPriceRequest;
use Ipost\SDK\Response\CourierReviewsIterator;
use Ipost\SDK\Response\CourierReviewsResponse;
use Ipost\SDK\Response\ErrorResponse;
use Ipost\SDK\Response\OrderCanceledResponse;
use Ipost\SDK\Response\OrderCreatedResponse;
use Ipost\SDK\Response\OrderReviewsIterator;
use Ipost\SDK\Response\OrderReviewsResponse;
use Ipost\SDK\Response\OrdersIterator;
use Ipost\SDK\Response\OrdersResponse;
use Ipost\SDK\Response\OrderUpdatedResponse;
use Ipost\SDK\Response\PaymentCardsIterator;
use Ipost\SDK\Response\PaymentCardsResponse;
use Ipost\SDK\Response\PreviewOrderResponse;
use Ipost\SDK\Utils\Hydrator;
use Ipost\SDK\Utils\Json;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class SDKClient
{
    private const DEFAULT_API_URL = 'https://api.ipost.life/public/v1';

    private ClientInterface $client;
    private LoggerInterface $logger;
    private Hydrator $hydrator;
    private string $apiUrl;
    private string $accessToken;

    public function __construct(
        string $accessToken,
        ClientInterface $client,
        ?string $apiUrl = null,
        ?LoggerInterface $logger = null
    ) {
        $this->accessToken = $accessToken;
        $this->client = $client;
        $this->apiUrl = $apiUrl ?? self::DEFAULT_API_URL;
        $this->logger = $logger ?? new NullLogger();
        $this->hydrator = new Hydrator();
    }

    /**
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws ApiErrorException
     */
    private function send(string $method, string $uri, ?RequestInterface $form = null): ResponseInterface
    {
        $request = new Request($method, $this->apiUrl . $uri, [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->accessToken,
        ], $form ? Json::encode($form) : null);

        try {
            $response = $this->client->sendRequest($request);
            $this->logger->debug('Body response', ['content' => $response->getBody()->getContents()]);
        } catch (ClientExceptionInterface $e) {
            $this->logger->error('Client', [
                'code' => $e->getCode(),
                'message' => $e->getMessage(),
                'file' => $e->getFile() . ':' . $e->getLine(),
            ]);
            throw $e;
        }

        if ($response->getStatusCode() === 422) {
            /** @var ErrorResponse $error */
            $error = $this->hydrator->fromResponse(ErrorResponse::class, $response);
            $this->logger->error('Api', (array) $error);
            switch ($error->code) {
                case ApiErrors::COMMON_ERROR:
                    throw new CommonException($error->message, $error->code);
                case ApiErrors::API_VERSION_CLOSED_ERROR:
                    throw new ApiVersionClosedException($error->message, $error->code);
                case ApiErrors::NOT_FOUND_ERROR:
                    throw new NotFoundException($error->message, $error->code);
                case ApiErrors::VALIDATION_ERROR:
                    throw new ValidationException($error->message, $error->code, $error->errors);
                case ApiErrors::NOT_ENOUGH_BALANCE_ERROR:
                    throw new NotEnoughBalanceException($error->message, $error->code, $error->balance);
                case ApiErrors::BALANCE_NEGATIVE_ERROR:
                    throw new BalanceNegativeException($error->message, $error->code, $error->balance);
                case ApiErrors::NOT_ALLOWED_BY_CONDITION_ERROR:
                    throw new NotAllowedByConditionException($error->message, $error->code);
                case ApiErrors::NO_MATCHING_TARIFF_FOUND_ERROR:
                    throw new NoMatchingTariffFoundException($error->message, $error->code, $error->maxLength, $error->maxWidth, $error->maxHeight, $error->maxWeight);
                case ApiErrors::IMPERMISSIBLE_VALUATION_ERROR:
                    throw new ImpermissibleValuationException($error->message, $error->code, $error->max);
                case ApiErrors::ORDER_ALREADY_INWORK_ERROR:
                    throw new OrderAlreadyInworkException($error->message, $error->code);
                case ApiErrors::ADDRESS_OUT_OF_MAP_BOUNDS_ERROR:
                    throw new AddressOutOfMapBoundsException($error->message, $error->code, $error->lat, $error->long, $error->reason);
                case ApiErrors::PROPERTY_IS_READONLY_ERROR:
                    throw new PropertyIsReadonlyException($error->message, $error->code);
                case ApiErrors::ADDRESSES_MATCH_ERROR:
                    throw new AddressesMatchException($error->message, $error->code);
                case ApiErrors::IMPERMISSIBLE_REDEEM_VALUATION_ERROR:
                    throw new ImpermissibleRedeemValuationException($error->message, $error->code, $error->max);
                case ApiErrors::IMPERMISSIBLE_DELIVERY_TIME_ERROR:
                    throw new ImpermissibleDeliveryTimeException($error->message, $error->code, $error->min);
                case ApiErrors::IMPERMISSIBLE_REDELIVERY_TIME_ERROR:
                    throw new ImpermissibleRedeliveryTimeException($error->message, $error->code, $error->min);
                case ApiErrors::IMPERMISSIBLE_CODTOACCOUNT_VALUATION_ERROR:
                    throw new ImpermissibleCodtoaccountValuationException($error->message, $error->code, $error->max);
                case ApiErrors::IMPERMISSIBLE_AGE_CHECK_DOB_ERROR:
                    throw new ImpermissibleAgeCheckDobException($error->message, $error->code);
                case ApiErrors::COMMENT_REQUIRED_ERROR:
                    throw new CommentRequiredException($error->message, $error->code);
                case ApiErrors::OPTION_DISABLED_ERROR:
                    throw new OptionDisabledException($error->message, $error->code);
                case ApiErrors::VOUCHER_NOT_FOUND_ERROR:
                    throw new VoucherNotFoundException($error->message, $error->code);
                case ApiErrors::CANNOT_PAY_WITH_BALANCE_WHEN_VOUCHER_USED_ERROR:
                    throw new CannotPayWithBalanceWhenVoucherUsedException($error->message, $error->code);
                case ApiErrors::PAYMENT_CARD_NOT_FOUND_ERROR:
                    throw new PaymentCardNotFoundException($error->message, $error->code);
                case ApiErrors::PAYMENT_FAILED_ERROR:
                    throw new PaymentFailedException($error->message, $error->code);
                default:
                    throw new ApiErrorException($error->message, $error->code);
            }
        } elseif ($response->getStatusCode() === 200) {
            return $response;
        }

        $this->logger->error('Http', [
            'code' => $response->getStatusCode(),
            'reason' => $response->getReasonPhrase(),
        ]);

        throw new ResponseException('Unexpected status code ' . $response->getStatusCode() . ' (' . $response->getReasonPhrase() . ')', $response->getStatusCode());
    }

    /**
     * @param ?int $date_from Фильтр по времени "от" (unix timestamp)
     * @param ?int $date_to Фильтр по времени "до" (unix timestamp)
     * @param ?string $status Фильтр по статусу (значение OrderStatusEnum, или active, или inactive)
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws ApiErrorException
     */
    public function getOrders(?int $date_from = null, ?int $date_to = null, ?string $status = null, ?int $page = null): OrdersIterator
    {
        $query = http_build_query([
            'date_from' => $date_from,
            'date_to' => $date_to,
            'status' => $status,
            'page' => $page,
        ]);

        $response = $this->send('GET', '/orders' . ($query ? '?' . $query : null));

        /** @var OrdersResponse $ordersResponse */
        $ordersResponse = $this->hydrator->fromResponse(OrdersResponse::class, $response);

        return new OrdersIterator($ordersResponse->items, $ordersResponse->meta);
    }

    /**
     * Получение подробной информации по заказу
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ApiErrorException
     */
    public function getOrder(int $order_id): Order
    {
        $response = $this->send('GET', '/orders/' . $order_id);

        return $this->hydrator->fromResponse(Order::class, $response);
    }

    /**
     * Для оценки стоимости заказа (без создания заказа)
     *
     * При запросе отменяются проверки на обязательность полей (required => non required).
     * Остальные проверки остаются, но могут быть пропущены, если для какой-то проверки не хватает данных (например,
     * если нет габаритов отправления или веса посылок, то проверка на наличие подходящего тарифа не будет произведена,
     * соответственно, ошибки 109 быть не может).
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ValidationException
     * @throws NoMatchingTariffFoundException
     * @throws ImpermissibleValuationException
     * @throws AddressOutOfMapBoundsException
     * @throws AddressesMatchException
     * @throws ImpermissibleRedeemValuationException
     * @throws ImpermissibleCodtoaccountValuationException
     * @throws ImpermissibleAgeCheckDobException
     * @throws OptionDisabledException
     * @throws VoucherNotFoundException
     * @throws CannotPayWithBalanceWhenVoucherUsedException
     * @throws PaymentCardNotFoundException
     * @throws ApiErrorException
     */
    public function previewOrder(OrderRequest $form): PreviewOrderResponse
    {
        $response = $this->send('POST', '/orders?preview=1', $form);

        return $this->hydrator->fromResponse(PreviewOrderResponse::class, $response);
    }

    /**
     * Создание заказа
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ValidationException
     * @throws NotEnoughBalanceException
     * @throws BalanceNegativeException
     * @throws NoMatchingTariffFoundException
     * @throws ImpermissibleValuationException
     * @throws AddressOutOfMapBoundsException
     * @throws AddressesMatchException
     * @throws ImpermissibleRedeemValuationException
     * @throws ImpermissibleDeliveryTimeException
     * @throws ImpermissibleRedeliveryTimeException
     * @throws ImpermissibleCodtoaccountValuationException
     * @throws ImpermissibleAgeCheckDobException
     * @throws OptionDisabledException
     * @throws VoucherNotFoundException
     * @throws CannotPayWithBalanceWhenVoucherUsedException
     * @throws PaymentCardNotFoundException
     * @throws PaymentFailedException
     * @throws ApiErrorException
     */
    public function createOrder(OrderRequest $form): OrderCreatedResponse
    {
        $response = $this->send('POST', '/orders', $form);

        return $this->hydrator->fromResponse(OrderCreatedResponse::class, $response);
    }

    /**
     * Изменение добавочной стоимости существующего заказа
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ValidationException
     * @throws OrderAlreadyInworkException
     * @throws PropertyIsReadonlyException
     * @throws ApiErrorException
     */
    public function updateOrder(int $order_id, UpdateCustomPriceRequest $form): OrderUpdatedResponse
    {
        $response = $this->send('PATCH', '/orders/' . $order_id, $form);

        return $this->hydrator->fromResponse(OrderUpdatedResponse::class, $response);
    }

    /**
     * Отмена заказа
     *
     * @param bool $preview для оценки штрафа без отмены
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ApiErrorException
     */
    public function cancelOrder(int $order_id, bool $preview = false): OrderCanceledResponse
    {
        $query = http_build_query([
            'preview' => $preview,
        ]);

        $response = $this->send('POST', '/orders/' . $order_id . '/cancel' . ($query ? '?' . $query : null));

        return $this->hydrator->fromResponse(OrderCanceledResponse::class, $response);
    }

    /**
     * Оставить отзыв об исполнении заказа
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ValidationException
     * @throws NotAllowedByConditionException
     * @throws CommentRequiredException
     * @throws ApiErrorException
     */
    public function addReview(int $order_id, ReviewRequest $form): bool
    {
        $response = $this->send('POST', '/orders/' . $order_id . '/review', $form);

        return $response->getStatusCode() === 200;
    }

    /**
     * Изменить отзыв об исполнении заказа
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ValidationException
     * @throws NotAllowedByConditionException
     * @throws CommentRequiredException
     * @throws ApiErrorException
     */
    public function updateReview(int $order_id, ReviewRequest $form): bool
    {
        $response = $this->send('PATCH', '/orders/' . $order_id . '/review', $form);

        return $response->getStatusCode() === 200;
    }

    /**
     * Удалить отзыв об исполнении заказа
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ApiErrorException
     */
    public function deleteReview(int $order_id): bool
    {
        $response = $this->send('DELETE', '/orders/' . $order_id . '/review');

        return $response->getStatusCode() === 200;
    }

    /**
     * Получить локацию курьера
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws ApiErrorException
     */
    public function getTracking(int $order_id): Location
    {
        $response = $this->send('GET', '/orders/' . $order_id . '/tracking');

        return $this->hydrator->fromResponse(Location::class, $response);
    }

    /**
     * Получение всех отзывов о курьере
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws NotFoundException
     * @throws NotAllowedByConditionException
     * @throws ApiErrorException
     */
    public function getCourierReviews(int $order_id): CourierReviewsIterator
    {
        $response = $this->send('GET', '/orders/' . $order_id . '/courier/reviews');

        /** @var CourierReviewsResponse $courierReviewsResponse */
        $courierReviewsResponse = $this->hydrator->fromResponse(CourierReviewsResponse::class, $response);

        return new CourierReviewsIterator($courierReviewsResponse->items, $courierReviewsResponse->meta);
    }

    /**
     * Получение всех отзывов на заказы
     *
     * @param ?int $order_id Фильтр по ID заказа
     * @param ?int $date_from Фильтр по времени "от" (unix timestamp)
     * @param ?int $date_to Фильтр по времени "до" (unix timestamp)
     * @param ?CourierReviewAuthorTypeEnum $author_type Фильтр по типу автора
     * @param ?string $sort Сортировка. Значение - имя поля. Только по примитивным полям.
     * Если перед именем поля есть минус (-), порядок сортировки будет DESC, иначе ASC. Сортировка по умолчанию - id DESC
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws ApiErrorException
     */
    public function getOrderReviews(
        ?int $order_id = null,
        ?int $date_from = null,
        ?int $date_to = null,
        ?CourierReviewAuthorTypeEnum $author_type = null,
        ?string $sort = null,
        ?int $page = null
    ): OrderReviewsIterator {
        $query = http_build_query([
            'date_from' => $date_from,
            'date_to' => $date_to,
            'order_id' => $order_id,
            'author_type' => $author_type,
            'sort' => $sort,
            'page' => $page,
        ]);

        $response = $this->send('GET', '/order-reviews' . ($query ? '?' . $query : null));

        /** @var OrderReviewsResponse $orderReviewsResponse */
        $orderReviewsResponse = $this->hydrator->fromResponse(OrderReviewsResponse::class, $response);

        return new OrderReviewsIterator($orderReviewsResponse->items, $orderReviewsResponse->meta);
    }

    /**
     * Получение списка привязанных платежных карт
     *
     * @throws ResponseException
     * @throws ClientExceptionInterface
     * @throws ApiErrorException
     */
    public function getPaymentCards(): PaymentCardsIterator
    {
        $response = $this->send('GET', '/cards');

        /** @var PaymentCardsResponse $paymentCardsResponse */
        $paymentCardsResponse = $this->hydrator->fromResponse(PaymentCardsResponse::class, $response);

        return new PaymentCardsIterator($paymentCardsResponse->items);
    }
}