<?php

declare(strict_types=1);

namespace Ipost\SDK\Utils;

use Psr\Http\Message\ResponseInterface;
use ReflectionClass;
use ReflectionProperty;

class Hydrator
{
    public function fromJson(string $className, string $json)
    {
        $array = Json::decode($json);

        return $this->fromArray($className, $array);
    }

    public function fromResponse(string $className, ResponseInterface $response)
    {
        $array = Json::decode((string) $response->getBody());

        return $this->fromArray($className, $array);
    }

    public function fromArray(string $className, array $data)
    {
        $object = new $className();

        $reflect = new ReflectionClass($object);
        $props = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);

        foreach ($props as $prop) {
            $value = $data[$prop->name] ?? null;
            $typeName = null;
            if ($type = $prop->getType()) {
                $typeName = $type->getName();
            }
            if ($value !== null) {
                if ($typeName === 'bool') {
                    $value = (bool) $value;
                } elseif ($typeName === 'int') {
                    $value = (int) $value;
                } elseif ($typeName === 'string') {
                    $value = (string) $value;
                } elseif ($typeName === 'float') {
                    $value = (float) $value;
                } elseif ($typeName === 'array' && $doc = $prop->getDocComment()) {
                    preg_match('/@var.+?\\\(.+?)\[/', $doc, $out);
                    if (isset($out[1]) && $typeName = $out[1]) {
                        foreach ($value as $k => $v) {
                            $value[$k] = is_array($value) ? $this->fromArray($typeName, $v) : new $typeName($v);
                        }
                    }
                } elseif (is_array($value)) {
                    $value = $this->fromArray($typeName, $value);
                } elseif ($typeName !== null) {
                    $value = new $typeName($value);
                }
            }
            $object->{$prop->name} = $value;
        }

        return $object;
    }
}