<?php

declare(strict_types=1);

namespace Ipost\SDK\Utils;

use Ipost\SDK\Exception\InvalidArgumentException;
use Throwable;

class Json
{
    /**
     * @throws InvalidArgumentException if the JSON cannot be decoded
     */
    public static function decode(string $json): array
    {
        try {
            return json_decode($json, true);
        } catch (Throwable $e) {
            throw new InvalidArgumentException('json_decode error: ' . $e->getMessage());
        }
    }

    /**
     * @throws InvalidArgumentException if the JSON cannot be encoded
     */
    public static  function encode($data): string
    {
        try {
            return json_encode($data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        } catch (Throwable $e) {
            throw new InvalidArgumentException('json_encode error: ' . $e->getMessage());
        }
    }
}