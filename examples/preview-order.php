<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Ipost\SDK\Constants\DeliveryTime;
use Ipost\SDK\Enum\FormOrderPaymentTypeEnum;
use Ipost\SDK\Exception\ApiError\AddressesMatchException;
use Ipost\SDK\Exception\ApiError\AddressOutOfMapBoundsException;
use Ipost\SDK\Exception\ApiError\ApiErrorException;
use Ipost\SDK\Exception\ApiError\CannotPayWithBalanceWhenVoucherUsedException;
use Ipost\SDK\Exception\ApiError\ImpermissibleAgeCheckDobException;
use Ipost\SDK\Exception\ApiError\ImpermissibleCodtoaccountValuationException;
use Ipost\SDK\Exception\ApiError\ImpermissibleRedeemValuationException;
use Ipost\SDK\Exception\ApiError\ImpermissibleValuationException;
use Ipost\SDK\Exception\ApiError\NoMatchingTariffFoundException;
use Ipost\SDK\Exception\ApiError\NotFoundException;
use Ipost\SDK\Exception\ApiError\OptionDisabledException;
use Ipost\SDK\Exception\ApiError\PaymentCardNotFoundException;
use Ipost\SDK\Exception\ApiError\ValidationException;
use Ipost\SDK\Exception\ApiError\VoucherNotFoundException;
use Ipost\SDK\Exception\ResponseException;
use Ipost\SDK\Request\OrderAddressRequest;
use Ipost\SDK\Request\OrderDeliveryRequest;
use Ipost\SDK\Request\OrderParcelRequest;
use Ipost\SDK\Request\OrderPickupRequest;
use Ipost\SDK\Request\OrderRequest;
use Ipost\SDK\SDKClient;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Client\ClientExceptionInterface;

require '../vendor/autoload.php';

$config = require 'config.php';

$logger = new Logger('iPOST.PreviewOrder');
$logger->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$sdkClient = new SDKClient($config['ipost_token'], new Client(), $config['ipost_api_url'], $logger);

$form = new OrderRequest(
    200,
    300,
    45,
    [
        new OrderDeliveryRequest(
            'Алексей',
            '380961234567',
            new OrderAddressRequest(
                50.4493657,
                30.6001729,
                'Киев',
                'ул. Евгения Сверстюка, 11'
            ),
            [
                new OrderParcelRequest(
                    'Книги',
                    250,
                    450,
                    1,
                ),
            ],
            '2555/12QWE'
        ),
    ],
    new OrderAddressRequest(
        50.5065182,
        30.4968440,
        'Киев',
        'Оболонский просп., 9А'
    ),
    new OrderPickupRequest(),
    FormOrderPaymentTypeEnum::PAYMENT_TYPE_BALANCE(),
    DeliveryTime::TIME_7200,
    'Книги',
    'Василий Галкин',
    '380961234567',
    null,
    null,
    null,
    10
);


try {
    $result = $sdkClient->previewOrder($form);
    echo "Стоимость доставки: " . $result->price;
    echo PHP_EOL . "Минимальное время доставки: " . $result->min_delivery_time;
} catch (NotFoundException $e) {
    echo "Нет привязанной карты (в случае наложенного платежа на карту клиента)";
} catch (ValidationException $e) {
    echo "Ошибка валидации:";
    foreach ($e->getErrors() as $error) {
        echo PHP_EOL . "- " . $error->message;
    }
} catch (AddressOutOfMapBoundsException $e) {
    echo "Указанный адрес находится вне допустимых пределах карты:";
    echo PHP_EOL . "Координаты: " . $e->getLat() . ", " . $e->getLong();
    $reason = $e->getReason();
    if ($reason) {
        echo PHP_EOL . "Причина: " . $e->getReason();
    }
} catch (AddressesMatchException $e) {
    echo "Адрес отправителя и адрес получателя находятся в разных населенных пунктах";
} catch (CannotPayWithBalanceWhenVoucherUsedException $e) {
    echo "Нельзя произвести оплату с баланса при использовании ваучера";
} catch (ImpermissibleAgeCheckDobException $e) {
    echo "Значение age_check_dob не соответствует совершеннолетию на дату доставки";
} catch (ImpermissibleCodtoaccountValuationException $e) {
    echo "Cумма оценочной стоимости для наложенного платежа на расчетный счет больше допустимой.";
    echo PHP_EOL . "Максимум: " . $e->getMax();
} catch (ImpermissibleRedeemValuationException $e) {
    echo "Cумма оценочной стоимости для выкупа больше допустимой.";
    echo PHP_EOL . "Максимум: " . $e->getMax();
} catch (ImpermissibleValuationException $e) {
    echo "Cумма оценочной стоимости больше допустимой.";
    echo PHP_EOL . "Максимум: " . $e->getMax();
} catch (NoMatchingTariffFoundException $e) {
    echo "Для заказа не нашлось подходящего тарифа.";
    echo PHP_EOL . "Максимальная длина (мм): " . $e->getMaxLength();
    echo PHP_EOL . "Максимальная ширина (мм): " . $e->getMaxWidth();
    echo PHP_EOL . "Максимальная высота (мм): " . $e->getMaxHeight();
    echo PHP_EOL . "Максимальный вес (г): " . $e->getMaxWeight();
} catch (OptionDisabledException $e) {
    echo "Опция отключена. " . $e->getMessage();
} catch (PaymentCardNotFoundException $e) {
    echo "Платежная карта не найдена";
} catch (VoucherNotFoundException $e) {
    echo "Ваучер не найден";
} catch (ApiErrorException $e) {
    echo "Ошибка API " . $e->getCode();
} catch (ResponseException $e) {
    echo $e->getMessage();
} catch (ClientExceptionInterface $e) {
    echo "Ошибка HTTP клиента";
}

echo PHP_EOL;