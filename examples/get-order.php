<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Ipost\SDK\Exception\ApiError\ApiErrorException;
use Ipost\SDK\Exception\ApiError\NotFoundException;
use Ipost\SDK\Exception\ResponseException;
use Ipost\SDK\SDKClient;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Client\ClientExceptionInterface;

require '../vendor/autoload.php';

$config = require 'config.php';

$logger = new Logger('iPOST.GetOrder');
$logger->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$sdkClient = new SDKClient($config['ipost_token'], new Client(), $config['ipost_api_url'], $logger);

$order_id = null;
if (isset($argv[1])) {
    $order_id = (int) $argv[1];
} elseif (isset($_GET['id'])) {
    $order_id = (int) $_GET['id'];
}

try {
    $order = $sdkClient->getOrder($order_id);
    print_r($order);
} catch (NotFoundException $e) {
    echo "Заказ не найден";
} catch (ApiErrorException $e) {
    echo "Ошибка API " . $e->getCode();
} catch (ResponseException $e) {
    echo $e->getMessage();
} catch (ClientExceptionInterface $e) {
    echo "Ошибка HTTP клиента";
}

echo PHP_EOL;