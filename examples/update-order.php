<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Ipost\SDK\Exception\ApiError\ApiErrorException;
use Ipost\SDK\Exception\ApiError\NotFoundException;
use Ipost\SDK\Exception\ApiError\OrderAlreadyInworkException;
use Ipost\SDK\Exception\ApiError\PropertyIsReadonlyException;
use Ipost\SDK\Exception\ApiError\ValidationException;
use Ipost\SDK\Exception\ResponseException;
use Ipost\SDK\Request\UpdateCustomPriceRequest;
use Ipost\SDK\SDKClient;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Client\ClientExceptionInterface;

require '../vendor/autoload.php';

$config = require 'config.php';

$logger = new Logger('iPOST.UpdateOrder');
$logger->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$sdkClient = new SDKClient($config['ipost_token'], new Client(), $config['ipost_api_url'], $logger);

$order_id = null;
if (isset($argv[1])) {
    $order_id = (int) $argv[1];
} elseif (isset($_GET['id'])) {
    $order_id = (int) $_GET['id'];
}

try {
    $sdkClient->updateOrder($order_id, new UpdateCustomPriceRequest(10));
} catch (NotFoundException $e) {
    echo "Заказ не найден";
} catch (ValidationException $e) {
    echo "Ошибка валидации:";
    foreach ($e->getErrors() as $error) {
        echo PHP_EOL . "- " . $error->message;
    }
} catch (OrderAlreadyInworkException $e) {
    echo "Статус заказа изменен (принят, отменен и т.п.)";
} catch (PropertyIsReadonlyException $e) {
    echo "Нельзя изменить надбавку для этого заказа";
} catch (ApiErrorException $e) {
    echo "Ошибка API " . $e->getCode();
} catch (ResponseException $e) {
    echo $e->getMessage();
} catch (ClientExceptionInterface $e) {
    echo "Ошибка HTTP клиента";
}

echo PHP_EOL;