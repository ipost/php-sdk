<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Ipost\SDK\Exception\ApiError\ApiErrorException;
use Ipost\SDK\Exception\ResponseException;
use Ipost\SDK\SDKClient;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Client\ClientExceptionInterface;

require '../vendor/autoload.php';

$config = require 'config.php';

$logger = new Logger('iPOST.GetPaymentCards');
$logger->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$sdkClient = new SDKClient($config['ipost_token'], new Client(), $config['ipost_api_url'], $logger);

try {
    $cards = $sdkClient->getPaymentCards();
    foreach ($cards as $card) {
        echo '[' . $card->type . ': ' . $card->number . ']' . PHP_EOL;
    }
} catch (ApiErrorException $e) {
    echo "Ошибка API " . $e->getCode();
} catch (ResponseException $e) {
    echo $e->getMessage();
} catch (ClientExceptionInterface $e) {
    echo "Ошибка HTTP клиента";
}

echo PHP_EOL;