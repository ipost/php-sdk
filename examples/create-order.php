<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Ipost\SDK\Enum\FormOrderTargetPaymentTypeEnum;
use Ipost\SDK\Exception\ApiError\AddressesMatchException;
use Ipost\SDK\Exception\ApiError\AddressOutOfMapBoundsException;
use Ipost\SDK\Exception\ApiError\ApiErrorException;
use Ipost\SDK\Exception\ApiError\BalanceNegativeException;
use Ipost\SDK\Exception\ApiError\CannotPayWithBalanceWhenVoucherUsedException;
use Ipost\SDK\Exception\ApiError\ImpermissibleAgeCheckDobException;
use Ipost\SDK\Exception\ApiError\ImpermissibleCodtoaccountValuationException;
use Ipost\SDK\Exception\ApiError\ImpermissibleDeliveryTimeException;
use Ipost\SDK\Exception\ApiError\ImpermissibleRedeemValuationException;
use Ipost\SDK\Exception\ApiError\ImpermissibleRedeliveryTimeException;
use Ipost\SDK\Exception\ApiError\ImpermissibleValuationException;
use Ipost\SDK\Exception\ApiError\NoMatchingTariffFoundException;
use Ipost\SDK\Exception\ApiError\NotEnoughBalanceException;
use Ipost\SDK\Exception\ApiError\NotFoundException;
use Ipost\SDK\Exception\ApiError\OptionDisabledException;
use Ipost\SDK\Exception\ApiError\PaymentCardNotFoundException;
use Ipost\SDK\Exception\ApiError\PaymentFailedException;
use Ipost\SDK\Exception\ApiError\ValidationException;
use Ipost\SDK\Exception\ApiError\VoucherNotFoundException;
use Ipost\SDK\Exception\ResponseException;
use Ipost\SDK\Request\OrderAddressRequest;
use Ipost\SDK\Request\OrderDeliveryRequest;
use Ipost\SDK\Request\OrderParcelRequest;
use Ipost\SDK\Request\OrderPickupRequest;
use Ipost\SDK\Request\OrderRequest;
use Ipost\SDK\SDKClient;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Client\ClientExceptionInterface;

require '../vendor/autoload.php';

$config = require 'config.php';

$logger = new Logger('iPOST.CreateOrder');
$logger->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$sdkClient = new SDKClient($config['ipost_token'], new Client(), $config['ipost_api_url'], $logger);

$form = new OrderRequest(
    400,
    300,
    45,
    [
        new OrderDeliveryRequest(
            'Алексей',
            '380961234567',
            new OrderAddressRequest(
                50.4493657,
                30.6001729,
                'Киев',
                'ул. Евгения Сверстюка, 11'
            ),
            [
                new OrderParcelRequest(
                    'Книги',
                    250,
                    450,
                    1,
                ),
            ],
            '2555/12QWE',
            null,
            time() + 1 * 60 * 60,
            time() + 2 * 60 * 60,
            null,
            null,
            FormOrderTargetPaymentTypeEnum::PAYMENT_TYPE_BALANCE(),
        ),
        new OrderDeliveryRequest(
            'Алексей',
            '380961234567',
            new OrderAddressRequest(
                50.4493657,
                30.6001729,
                'Киев',
                'ул. Евгения Сверстюка, 12'
            ),
            [
                new OrderParcelRequest(
                    'Книги',
                    250,
                    450,
                    1,
                ),
            ],
            '2555/12QWE',
            null,
            time() + 1 * 60 * 60,
            time() + 2 * 60 * 60,
            null,
            null,
            FormOrderTargetPaymentTypeEnum::PAYMENT_TYPE_SENDER(),
        ),
    ],
    new OrderAddressRequest(
        50.5065182,
        30.4968440,
        'Киев',
        'Оболонский просп., 9А'
    ),
    new OrderPickupRequest(
        time() + 1 * 60 * 60,
        time() + 2 * 60 * 60
    ),
    null,
    null,
    'Книги',
    'Василий Галкин',
    '380961234567'
);


try {
    $result = $sdkClient->createOrder($form);
    echo "Заказ успешно создан! ID заказа {$result->id}.";
} catch (NotFoundException $e) {
    echo "Нет привязанной карты (в случае наложенного платежа на карту клиента)";
} catch (ValidationException $e) {
    echo "Ошибка валидации:";
    foreach ($e->getErrors() as $error) {
        echo PHP_EOL . "- " . $error->message;
    }
} catch (AddressOutOfMapBoundsException $e) {
    echo "Указанный адрес находится вне допустимых пределах карты:";
    echo PHP_EOL . "Координаты: " . $e->getLat() . ", " . $e->getLong();
    $reason = $e->getReason();
    if ($reason) {
        echo PHP_EOL . "Причина: " . $e->getReason();
    }
} catch (AddressesMatchException $e) {
    echo "Адрес отправителя и адрес получателя находятся в разных населенных пунктах";
} catch (BalanceNegativeException $e) {
    echo "Баланс ниже нуля.";
    echo PHP_EOL . "Текущий баланс: " . $e->getBalance();
} catch (CannotPayWithBalanceWhenVoucherUsedException $e) {
    echo "Нельзя произвести оплату с баланса при использовании ваучера";
} catch (ImpermissibleAgeCheckDobException $e) {
    echo "Значение age_check_dob не соответствует совершеннолетию на дату доставки";
} catch (ImpermissibleCodtoaccountValuationException $e) {
    echo "Cумма оценочной стоимости для наложенного платежа на расчетный счет больше допустимой.";
    echo PHP_EOL . "Максимум: " . $e->getMax();
} catch (ImpermissibleDeliveryTimeException $e) {
    echo "Время доставки слишком малое для этого маршрута.";
    echo PHP_EOL . "Минимум: " . $e->getMin();
} catch (ImpermissibleRedeemValuationException $e) {
    echo "Cумма оценочной стоимости для выкупа больше допустимой.";
    echo PHP_EOL . "Максимум: " . $e->getMax();
} catch (ImpermissibleRedeliveryTimeException $e) {
    echo "Время обратной доставки слишком малое для этого маршрута.";
    echo PHP_EOL . "Минимум: " . $e->getMin();
} catch (ImpermissibleValuationException $e) {
    echo "Cумма оценочной стоимости больше допустимой.";
    echo PHP_EOL . "Максимум: " . $e->getMax();
} catch (NoMatchingTariffFoundException $e) {
    echo "Для заказа не нашлось подходящего тарифа.";
    echo PHP_EOL . "Максимальная длина (мм): " . $e->getMaxLength();
    echo PHP_EOL . "Максимальная ширина (мм): " . $e->getMaxWidth();
    echo PHP_EOL . "Максимальная высота (мм): " . $e->getMaxHeight();
    echo PHP_EOL . "Максимальный вес (г): " . $e->getMaxWeight();
} catch (NotEnoughBalanceException $e) {
    echo "Недостаточно средств на балансе.";
    echo PHP_EOL . "Текущий баланс: " . $e->getBalance();
} catch (OptionDisabledException $e) {
    echo "Опция отключена. " . $e->getMessage();
} catch (PaymentCardNotFoundException $e) {
    echo "Платежная карта не найдена";
} catch (PaymentFailedException $e) {
    echo "Неуспешный платеж";
} catch (VoucherNotFoundException $e) {
    echo "Ваучер не найден";
} catch (ApiErrorException $e) {
    echo "Ошибка API " . $e->getCode();
} catch (ResponseException $e) {
    echo $e->getMessage();
} catch (ClientExceptionInterface $e) {
    echo "Ошибка HTTP клиента";
}

echo PHP_EOL;
