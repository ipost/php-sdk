<?php

declare(strict_types=1);

use Ipost\SDK\Enum\NotifyOrderStatusEnum;
use Ipost\SDK\Exception\NotifyException;
use Ipost\SDK\SDKNotify;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

require '../vendor/autoload.php';

$config = require 'config.php';

$logger = new Logger('iPOST.Notify');
$logger->pushHandler(new StreamHandler('log.log', Logger::WARNING));

$sdkNotify = new SDKNotify($config['ipost_token'], $logger);

try {
    $notify = $sdkNotify->getOrderStatusUpdated();
    if ($notify->status->equals(NotifyOrderStatusEnum::INWORK())) {
        $logger->info('Notify', ['status INWORK']);
        $logger->info('Notify', [
            'order_id' => $notify->order_id,
            'message' => 'Заказ в настоящее время обрабатывается.',
        ]);
    }
} catch (NotifyException $e) {
}
